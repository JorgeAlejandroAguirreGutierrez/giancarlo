import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { HomeLayoutComponent } from './layouts/home-layout/home-layout.component';
import { AsignarTecnicoOrdenComponent } from './pages/asignar-tecnico-orden/asignar-tecnico-orden.component';
import { AsignarRecursoComponent } from './pages/asignar-recurso/asignar-recurso.component';
import { SolicitudComponent } from './pages/solicitud/solicitud.component';
import { OrdenServicioComponent } from './pages/orden-servicio/orden-servicio.component';
import { SolicitudAprobacionComponent } from './pages/solicitud-aprobacion/solicitud-aprobacion.component';
import { GenerarHojaRutaComponent } from './pages/generar-hoja-ruta/generar-hoja-ruta.component';

const routes: Routes = [
  {
    path: '', component: HomeLayoutComponent,
    children:[
      {path: '', redirectTo: '/solicitud-servicio', pathMatch: 'full'},
      {path: 'asignar-tecnico-orden', component: AsignarTecnicoOrdenComponent},
      {path: 'asignar-recurso', component: AsignarRecursoComponent},
      {path: 'solicitud-servicio', component: SolicitudComponent},
      {path: 'solicitud-aprobacion', component: SolicitudAprobacionComponent},
      {path: 'orden-servicio', component: OrdenServicioComponent},
      {path: 'generar-hoja-ruta', component: GenerarHojaRutaComponent}
    ]
}
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
