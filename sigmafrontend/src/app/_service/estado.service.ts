import { HttpClient } from '@angular/common/http';
import { HOST } from './../_shared/var.constant';
import { Injectable } from '@angular/core';
import { map, catchError, switchAll } from 'rxjs/operators';
import { of, Observable, throwError } from 'rxjs';
import { Estado } from '../_model/estado';

@Injectable({
  providedIn: 'root'
})
export class EstadoService {

  url = `${HOST}/estados`;

  constructor(private http: HttpClient) { }

  listarEstado(): Observable<Estado[]> {
    return this.http.get<Estado[]>(this.url).pipe(
      map(response => response as Estado[]),
      catchError(err => {
        return throwError(err);
      })
    );
  }
}
