import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { HOST } from '../_shared/var.constant';
import { map, catchError, switchAll } from 'rxjs/operators';
import { of, Observable, throwError } from 'rxjs';
import {OrdenServicio} from '../_model/orden-servicio';
import { Busqueda } from '../_model/busqueda';
import { OrdenServicioRecurso } from '../_model/orden-servicio-recurso';
import { OrdenServicioTecnico } from '../_model/orden-servicio-tecnico';

@Injectable({
  providedIn: 'root'
})
export class OrdenServicioService {

  url = `${HOST}/ordenServicios`;
  url_recursos = `${HOST}/ordenServicioRecursos`;
  url_tecnicos = `${HOST}/ordenServicioTecnicos`;

  constructor(private http: HttpClient) { }

  crearOrdenServicio(ordenServicio: OrdenServicio): Observable<OrdenServicio> {
    return this.http.post<OrdenServicio>(this.url, ordenServicio).pipe(
      map(response => response as OrdenServicio),
      catchError(err => {
        return throwError(err);
      })
    );
  }

  crearOrdenServicioRecurso(ordenServicioRecurso: OrdenServicioRecurso): Observable<OrdenServicioRecurso> {
    return this.http.post<OrdenServicioRecurso>(this.url_recursos, ordenServicioRecurso).pipe(
      map(response => response as OrdenServicioRecurso),
      catchError(err => {
        return throwError(err);
      })
    );
  }

  actualizarOrdenServicioRecurso(ordenServicioRecurso: OrdenServicioRecurso): Observable<OrdenServicioRecurso> {
    return this.http.put<OrdenServicioRecurso>(this.url_recursos, ordenServicioRecurso).pipe(
      map(response => response as OrdenServicioRecurso),
      catchError(err => {
        return throwError(err);
      })
    );
  }

  crearOrdenServicioTecnico(ordenServicioTecnico: OrdenServicioTecnico): Observable<OrdenServicioTecnico> {
    return this.http.post<OrdenServicioTecnico>(this.url_tecnicos, ordenServicioTecnico).pipe(
      map(response => response as OrdenServicioTecnico),
      catchError(err => {
        return throwError(err);
      })
    );
  }

  actualizarOrdenServicioTecnico(ordenServicioTecnico: OrdenServicioTecnico): Observable<OrdenServicioTecnico> {
    return this.http.put<OrdenServicioTecnico>(this.url_tecnicos, ordenServicioTecnico).pipe(
      map(response => response as OrdenServicioTecnico),
      catchError(err => {
        return throwError(err);
      })
    );
  }

  actualizarOrdenServicio(ordenServicio: OrdenServicio): Observable<OrdenServicio> {
    return this.http.put<OrdenServicio>(this.url, ordenServicio).pipe(
      map(response => response as OrdenServicio),
      catchError(err => {
        return throwError(err);
      })
    );
  }

  listarOrdenServicio(busqueda: Busqueda): Observable<any> {
    let query ='skip='+busqueda.skip+'&take='+busqueda.take;
    if (busqueda.numero_orden != null) {
      query=query+'&numero_orden='+busqueda.numero_orden;
    }
    if (busqueda.fecha_recepcion != null) {
      query=query+'&fecha_recepcion='+busqueda.fecha_recepcion;
    }
    if (busqueda.id_tipo_solicitud != 0) {
      query=query+'&id_tipo_solicitud='+busqueda.id_tipo_solicitud;
    }
    if (busqueda.id_estado != 0) {
      query=query+'&id_estado='+busqueda.id_estado;
    }
    return this.http.get<any>(this.url + '/listarBusqueda?'+query).pipe(
      map(response => response as any),
      catchError(err => {
        return throwError(err);
      })
    );
  }

  listarOrdenServicioId(id) {
    return this.http.get(this.url + '/' + id);
  }

  eliminarOrdenServicio(id) {
    return this.http.delete(this.url + '/' + id);
  }

}
