import { of, Observable, throwError } from 'rxjs';
import { HttpClient } from '@angular/common/http';
import { HOST } from '../_shared/var.constant';
import { Injectable } from '@angular/core';
import { map, catchError, switchAll } from 'rxjs/operators';
import {ComplejidadServicio} from '../_model/complejidad-servicio';

@Injectable({
  providedIn: 'root'
})
export class ComplejidadServicioService {

  url = `${HOST}/complejidadesServicio`;

  constructor(private http: HttpClient) { }

  listarComplejidadServicio(): Observable<ComplejidadServicio[]> {
    return this.http.get<ComplejidadServicio[]>(this.url).pipe(
      map(response => response as ComplejidadServicio[]),
      catchError(err => {
        return throwError(err);
      })
    );
  }
}
