import { HttpClient } from '@angular/common/http';
import { HOST } from './../_shared/var.constant';
import { Injectable } from '@angular/core';
import { map, catchError, switchAll } from 'rxjs/operators';
import { of, Observable, throwError } from 'rxjs';
import {Tecnico} from '../_model/tecnico';

@Injectable({
  providedIn: 'root'
})
export class TecnicoService {
  url = `${HOST}/tecnicos`;

  constructor(private http: HttpClient) { }

  listarTecnicoAlgoritmo(idSolicitud): Observable<any> {
    return this.http.get(this.url + '/listarTecnicoHeuristica/' + idSolicitud);
  }

  listarTecnicos(): Observable<Tecnico[]> {
    return this.http.get<Tecnico[]>(this.url).pipe(
      map(response => response as Tecnico[]),
      catchError(err => {
        return throwError(err);
      })
    );
  }

  listarTecnicoId(id: number): Observable<Tecnico> {
    return this.http.get(this.url + '/' + id).pipe(
      map(response => response as Tecnico),
      catchError(err => {
        return throwError(err);
      })
    );
  }
}
