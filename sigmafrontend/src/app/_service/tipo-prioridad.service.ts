import { of, Observable, throwError } from 'rxjs';
import { HttpClient } from '@angular/common/http';
import { HOST } from '../_shared/var.constant';
import { Injectable } from '@angular/core';
import { map, catchError, switchAll } from 'rxjs/operators';
import {TipoPrioridad} from '../_model/tipo-prioridad';

@Injectable({
  providedIn: 'root'
})
export class TipoPrioridadService {

  url = `${HOST}/tipoPrioridades`;

  constructor(private http: HttpClient) { }

  listarTipoPrioridad(): Observable<TipoPrioridad[]> {
    return this.http.get<TipoPrioridad[]>(this.url).pipe(
      map(response => response as TipoPrioridad[]),
      catchError(err => {
        return throwError(err);
      })
    );
  }
}
