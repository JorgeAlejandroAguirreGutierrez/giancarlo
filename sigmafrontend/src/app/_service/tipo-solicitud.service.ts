import { of, Observable, throwError } from 'rxjs';
import { HttpClient } from '@angular/common/http';
import { HOST } from '../_shared/var.constant';
import { Injectable } from '@angular/core';
import { map, catchError, switchAll } from 'rxjs/operators';
import {TipoSolicitud} from '../_model/tipo-solicitud';

@Injectable({
  providedIn: 'root'
})
export class TipoSolicitudService {

  url = `${HOST}/tipoSolicitudes`;

  constructor(private http: HttpClient) { }

  listarTipoSolicitud(): Observable<TipoSolicitud[]> {
    return this.http.get<TipoSolicitud[]>(this.url).pipe(
      map(response => response as TipoSolicitud[]),
      catchError(err => {
        return throwError(err);
      })
    );
  }

  listarSubTipoSolicitud(id_sub_tipo_solicitud): Observable<any> {
    return this.http.get(this.url+'/'+id_sub_tipo_solicitud);
  }
}
