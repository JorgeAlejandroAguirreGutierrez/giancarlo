import { of, Observable, throwError } from 'rxjs';
import { HttpClient } from '@angular/common/http';
import { HOST } from '../_shared/var.constant';
import { Injectable } from '@angular/core';
import { map, catchError, switchAll } from 'rxjs/operators';
import {Recurso} from '../_model/recurso';

@Injectable({
  providedIn: 'root'
})
export class RecursoService {

  url = `${HOST}/recursos`;

  constructor(private http: HttpClient) { }

  listarRecursos(): Observable<Recurso[]> {
    return this.http.get<Recurso[]>(this.url).pipe(
      map(response => response as Recurso[]),
      catchError(err => {
        return throwError(err);
      })
    );
  }
}
