import { of, Observable, throwError } from 'rxjs';
import { HttpClient } from '@angular/common/http';
import { HOST } from '../_shared/var.constant';
import { Injectable } from '@angular/core';
import { map, catchError, switchAll } from 'rxjs/operators';
import {Zona} from '../_model/zona';

@Injectable({
  providedIn: 'root'
})
export class ZonaService {

  url = `${HOST}/zonas`;

  constructor(private http: HttpClient) { }

  listarZona(): Observable<Zona[]> {
    return this.http.get<Zona[]>(this.url).pipe(
      map(response => response as Zona[]),
      catchError(err => {
        return throwError(err);
      })
    );
  }
}
