import { HttpParams, HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { HOST } from '../_shared/var.constant';
import { Solicitud } from '../_model/solicitud';
import { map, catchError, switchAll } from 'rxjs/operators';
import { of, Observable, throwError } from 'rxjs';
import {SolicitudRecurso} from '../_model/solicitud-recurso';
import {SolicitudTecnico} from '../_model/solicitud-tecnico';
import {Busqueda} from '../_model/busqueda';

@Injectable({
  providedIn: 'root'
})
export class SolicitudService {

  url = `${HOST}/solicitudes`;
  url_recursos = `${HOST}/solicitudRecurso`;
  url_tecnicos = `${HOST}/solicitudTecnico`;

  constructor(private http: HttpClient) { }

  crearSolicitud(solicitud: Solicitud): Observable<Solicitud> {
    return this.http.post<Solicitud>(this.url, solicitud).pipe(
      map(response => response as Solicitud),
      catchError(err => {
        return throwError(err);
      })
    );
  }

  actualizarSolicitud(solicitud: Solicitud): Observable<Solicitud> {
    return this.http.put<Solicitud>(this.url, solicitud).pipe(
      map(response => response as Solicitud),
      catchError(err => {
        return throwError(err);
      })
    );
  }

  eliminarSolicitud(solicitud: Solicitud): Observable<Solicitud> {
    return this.http.delete<Solicitud>(this.url+'/'+solicitud.id_solicitud).pipe(
      map(response => response as Solicitud),
      catchError(err => {
        return throwError(err);
      })
    );
  }

  crearSolicitudRecurso(solicitudRecurso: SolicitudRecurso): Observable<SolicitudRecurso> {
    return this.http.post<SolicitudRecurso>(this.url_recursos, solicitudRecurso).pipe(
      map(response => response as SolicitudRecurso),
      catchError(err => {
        return throwError(err);
      })
    );
  }

  actualizarSolicitudRecurso(solicitudRecurso: SolicitudRecurso): Observable<SolicitudRecurso> {
    return this.http.put<SolicitudRecurso>(this.url_recursos, solicitudRecurso).pipe(
      map(response => response as SolicitudRecurso),
      catchError(err => {
        return throwError(err);
      })
    );
  }

  crearSolicitudTecnico(solicitudTecnico: SolicitudTecnico): Observable<SolicitudTecnico> {
    return this.http.post<SolicitudTecnico>(this.url_tecnicos, solicitudTecnico).pipe(
      map(response => response as SolicitudTecnico),
      catchError(err => {
        return throwError(err);
      })
    );
  }

  actualizarSolicitudTecnico(solicitudTecnico: SolicitudTecnico): Observable<SolicitudTecnico> {
    return this.http.put<SolicitudTecnico>(this.url_tecnicos, solicitudTecnico).pipe(
      map(response => response as SolicitudTecnico),
      catchError(err => {
        return throwError(err);
      })
    );
  }

  listarSolicitud(busqueda: Busqueda): Observable<any> {
    let query ='skip='+busqueda.skip+'&take='+busqueda.take;
    if (busqueda.numero_solicitud != null) {
      query=query+'&numero_solicitud='+busqueda.numero_solicitud;
    }
    if (busqueda.fecha_recepcion != null) {
      query=query+'&fecha_recepcion='+busqueda.fecha_recepcion;
    }
    if (busqueda.id_tipo_solicitud != 0) {
      query=query+'&id_tipo_solicitud='+busqueda.id_tipo_solicitud;
    }
    if (busqueda.id_estado != 0) {
      query=query+'&id_estado='+busqueda.id_estado;
    }
    return this.http.get<any>(this.url + '/listarBusqueda?'+query).pipe(
      map(response => response as any),
      catchError(err => {
        return throwError(err);
      })
    );
  }

  listarSolicitudId(id: number) {
    return this.http.get<Solicitud>(this.url + '/' + id).pipe(
      map(response => response as Solicitud),
      catchError(err => {
        return throwError(err);
      })
    );
  }

  listarSolicitudPagNum(numero_solicitud: string, fecha_recepcion: string, id_tipo_solicitud: string, id_estado: string, skip: string, take:string): Observable<Solicitud[]> {
    let params = new HttpParams();
    params.set('numero_solicitud', numero_solicitud);
    params.set('fecha_recepcion', fecha_recepcion);
    params.set('id_tipo_solicitud', id_tipo_solicitud);
    params.set('id_estado', id_estado);
    params.set('skip', skip);
    params.set('take', take);
    return this.http.get<Solicitud[]>(this.url + '/listarSolicitudServicioPagNum',{params: params}).pipe(
      map(response => response as Solicitud[]),
      catchError(err => {
        return throwError(err);
      })
    );
  }
}
