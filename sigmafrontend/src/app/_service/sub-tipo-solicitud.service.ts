import { of, Observable, throwError } from 'rxjs';
import { HttpClient } from '@angular/common/http';
import { HOST } from '../_shared/var.constant';
import { Injectable } from '@angular/core';
import { map, catchError, switchAll } from 'rxjs/operators';
import {SubTipoSolicitud} from '../_model/sub-tipo-solicitud';

@Injectable({
  providedIn: 'root'
})
export class SubTipoSolicitudService {

  url = `${HOST}/subTipoSolicitudes`;

  constructor(private http: HttpClient) { }

  listarSubTipoSolicitud(): Observable<SubTipoSolicitud[]> {
    return this.http.get<SubTipoSolicitud[]>(this.url).pipe(
      map(response => response as SubTipoSolicitud[]),
      catchError(err => {
        return throwError(err);
      })
    );
  }
}