import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { HOST } from '../_shared/var.constant';
import { ParametroSistema } from '../_model/parametro-sistema';
import { map, catchError, switchAll } from 'rxjs/operators';
import { of, Observable, throwError } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class ParametroSistemaService{

  url = `${HOST}/parametroSistema`;

  constructor(private http: HttpClient) { }

  listarParametroServicio(): Observable<any> {
    return this.http.get(this.url);
  }

  listarParametroServicioId(id) {
    return this.http.get(this.url + '/' + id);
  }

  listarParametrosTipo(id_tipo_solicitud: number, id_sub_tipo_solicitud: number): Observable<any> {
    let query ='id_tipo_solicitud='+id_tipo_solicitud+'&id_sub_tipo_solicitud='+id_sub_tipo_solicitud;
    return this.http.get<ParametroSistema[]>(this.url + '/listar/tipo?'+query).pipe(
      map(response => response as ParametroSistema[]),
      catchError(err => {
        return throwError(err);
      })
    );
  }

}