import { Component, OnInit, Output, EventEmitter } from '@angular/core';

import { Recurso } from 'src/app/_model/recurso';
import { BsModalRef } from 'ngx-bootstrap/modal';
import { OrdenServicioService } from 'src/app/_service/orden-servicio.service';
import { OrdenServicioRecurso } from 'src/app/_model/orden-servicio-recurso';
import { OrdenServicio } from 'src/app/_model/orden-servicio';
import { Funciones } from 'src/app/_shared/funciones';
import Swal from 'sweetalert2';
import { RecursoService } from 'src/app/_service/recurso.service';
import { ParametroSistema } from 'src/app/_model/parametro-sistema';

@Component({
  selector: 'app-modal-asignar-recurso',
  templateUrl: './modal-asignar-recurso.component.html',
  styleUrls: ['./modal-asignar-recurso.component.scss']
})
export class ModalAsignarRecursoComponent implements OnInit {

  ordenServicio: OrdenServicio;
  listaRecursos: any[];
  cu_ordenServicioRecurso = new OrdenServicioRecurso();
  listaParametrosRecursos: ParametroSistema[]=[];

  id_recurso: number;

  @Output()
  retorno = new EventEmitter();


  constructor(
    private recursoService: RecursoService,
    private modalRef: BsModalRef,
    private ordenServicioService: OrdenServicioService,
    public funciones: Funciones
  ) { }

  ngOnInit() {
    this.listarRecursos();
    this.eliminarRepetidos();
  }

  eliminarRepetidos() {
    for (let i=0; i<this.ordenServicio.solicitud.parametroSistema.length; i++) {
      let bandera=false;
        for (let j=0; j<this.listaParametrosRecursos.length; j++) {
          if (this.listaParametrosRecursos[j].recurso==this.ordenServicio.solicitud.parametroSistema[i].recurso) {
            bandera=true;
          }
        }
        if (!bandera) {
          this.listaParametrosRecursos.push(this.ordenServicio.solicitud.parametroSistema[i]);
        }
    }
  }

  eliminar(i: number) {
    Swal.fire({
      title: 'Asignacion',
      text: "Deseas eliminar el recurso",
      type: 'warning',
      showCancelButton: true,
      confirmButtonColor: '#3085d6',
      cancelButtonColor: '#d33',
      confirmButtonText: 'SI'
    }).then((result) => {
      if (result.value == true) {
        this.ordenServicio.ordenServicioRecursos.splice(i, 1);
      }
    });

  }

  guardarAsignarRecursos() {
    Swal.fire({
      title: 'Asignacion',
      text: "Deseas asignar recursos",
      type: 'warning',
      showCancelButton: true,
      confirmButtonColor: '#3085d6',
      cancelButtonColor: '#d33',
      confirmButtonText: 'SI'
    }).then((result) => {
      if (result.value == true) {
        this.actualizarOrdenServicio();
        
      }
    });
  }


  validarAsignacionRecursos() {
    let bandera = 0
    this.ordenServicio.ordenServicioRecursos.forEach(elemento1 => {
      this.ordenServicio.solicitud.parametroSistema.forEach(elemento2 => {
        if (elemento1.recurso.nombre == elemento2.recurso) {
          bandera = bandera + 1;
        }
      });
    });
    return bandera;
  }

  actualizarOrdenServicio() {
    this.ordenServicio.estado.id_estado = 6;
    this.ordenServicioService.actualizarOrdenServicio(this.ordenServicio).subscribe(
      data => {
        Swal.fire('Exito', 'Se ha asignado exitosamente los recursos', 'success');
        this.closeModal();
      },
      (err) => {
        Swal.fire('Error', 'No se pudo asignar recursos a la orden de servicio', 'error');
      }
    );
  }

  listarRecursos() {
    this.recursoService.listarRecursos().subscribe(
      data => {
        this.listaRecursos = data as Recurso[];
      }
    );
  }

  agregarRecurso() {
    let recurso = this.obtenerRecurso(this.id_recurso);
    let bandera = false;
    for (let i=0; i<this.ordenServicio.solicitud.parametroSistema.length; i++) {
      if (this.ordenServicio.solicitud.parametroSistema[i].recurso == recurso.nombre) {
        this.cu_ordenServicioRecurso=new OrdenServicioRecurso();
        this.cu_ordenServicioRecurso.recurso=recurso;
        this.ordenServicio.ordenServicioRecursos.push(this.cu_ordenServicioRecurso);
        bandera = true;
        break;
      }
    }
    if (!bandera) {
      Swal.fire('Error', 'Recurso no asignable', 'error');
    }
  }

  obtenerRecurso(idRecurso: number) {
    for (let i = 0; i < this.listaRecursos.length; i++) {
      if (this.listaRecursos[i].idRecurso == idRecurso) {
        return this.listaRecursos[i];
      }
    }
  }

  closeModal() {
    this.retorno.emit('0');
    this.modalRef.hide();
  }

}
