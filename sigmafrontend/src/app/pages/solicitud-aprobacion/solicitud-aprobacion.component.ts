import { Component, OnInit } from '@angular/core';
import { BsModalRef, BsModalService } from 'ngx-bootstrap/modal';
import { Busqueda } from 'src/app/_model/busqueda';
import { Solicitud } from 'src/app/_model/solicitud';
import { SolicitudService } from 'src/app/_service/solicitud.service';
import { TipoPrioridadService } from 'src/app/_service/tipo-prioridad.service';
import { TipoSolicitudService } from 'src/app/_service/tipo-solicitud.service';
import { OrdenServicioService } from 'src/app/_service/orden-servicio.service';
import { EstadoService } from 'src/app/_service/estado.service';
import { OrdenServicio } from 'src/app/_model/orden-servicio';
import { ModalVisualizacionSolicitudComponent } from './modal-visualizacion-solicitud/modal-visualizacion-solicitud.component';
import Swal from 'sweetalert2';
import { ParametroSistemaService } from 'src/app/_service/parametro-sistema.service';
import { ParametroSistema } from 'src/app/_model/parametro-sistema';

@Component({
  selector: 'app-solicitud-aprobacion',
  templateUrl: './solicitud-aprobacion.component.html',
  styleUrls: ['./solicitud-aprobacion.component.scss']
})
export class SolicitudAprobacionComponent implements OnInit {

  listaSolicitudes: any = [];
  listaEstadoSolicitud: any = [];
  listaEstado: any = [];
  listaTipoSolicitud: any = [];
  listaTipoPrioridad: any = [];
  listaParametros: any = [];

  numOrdenMask = ['S', 'O', 'L', '-', /[0-9]/, /\d/, /\d/, /\d/, /\d/, /\d/];

  totalRegistros = 0;
  totalfilasPorPagina = 0;
  paginaActiva = 0;
  numeroPagina = 5;
  numFilas = 5;
  numPaginasMostrar = 5;
  paginaActual: number = 0;

  config;
  bsModal: BsModalRef;
  busqueda: Busqueda;

  cu_solicitud: Solicitud;
  cu_ordenServicio=new OrdenServicio();
  orden_servicio_creada: OrdenServicio;

  constructor(
    private solicitudServicio: SolicitudService,
    private ordenServicioService: OrdenServicioService,
    private modalService: BsModalService,
    private servicioTipoPrioridad: TipoPrioridadService,
    private servicioTipoSolicitud: TipoSolicitudService,
    private servicioEstado: EstadoService,
    private parametroSistemaService: ParametroSistemaService
    
  ) { }

  ngOnInit() {
    this.busqueda = new Busqueda();
    this.ConsultaPrincipal(this.busqueda);
    this.listarTipoSolicitud();
    this.listarEstado();
  }

  aprobarOrdenServicio(solicitud: Solicitud) {
    this.aprobar(solicitud);
  }

  actualizarSolicitudAprobar(solicitud: Solicitud) {
    solicitud.estado.id_estado=3;
    this.solicitudServicio.actualizarSolicitud(solicitud).subscribe(
      data => {
        Swal.fire(
          'Aprobado',
          'Se ha aprobado la solicitud correctamente.',
          'success'
        );
        this.ConsultaPrincipal(new Busqueda());
      },
      (err) => {
        Swal.fire('Error', 'No se pudo aprobar la solicitud', 'error');
      }
    );
  }

  actualizarSolicitudDesaprobar(solicitud: Solicitud) {
    solicitud.estado.id_estado=8;
    solicitud.activo=false;
    this.solicitudServicio.actualizarSolicitud(solicitud).subscribe(
      data => {
        Swal.fire(
          'Rechazado',
          'Se ha rechazado la solicitud correctamente.',
          'success'
        );
        this.ConsultaPrincipal(new Busqueda());
      },
      (err) => {
        Swal.fire('Error', 'No se pudo desaprobar la solicitud', 'error');
      }
    );
  }

  openModalVisualizacion(solicitud: Solicitud) {
    this.config = {
      //ignoreBackdropClick: true,
      //keyboard: false,
      animated: true,
      class: 'modal-extra',
      initialState: {
        cu_solicitud: solicitud,
      }
    };
    this.bsModal = this.modalService.show(ModalVisualizacionSolicitudComponent, this.config);
    this.bsModal.content.retorno.subscribe(
      data => {
        this.ConsultaPrincipal(new Busqueda());
      }
    );
  }

  aprobar(solicitud: Solicitud) {
    Swal.fire({
      title: 'Aprobar',
      text: "Deseas aprobar esta solicitud de servicio",
      type: 'warning',
      showCancelButton: true,
      confirmButtonColor: '#3085d6',
      cancelButtonColor: '#d33',
      confirmButtonText: 'SI'
    }).then((result) => {
      console.log(confirm);
      if (result.value==true) {
        this.actualizarSolicitudAprobar(solicitud);
      }
    });
  }

  desaprobar(solicitud: Solicitud) {
    Swal.fire({
      title: 'Rechazar',
      text: "Deseas rechazar esta solicitud de servicio? Ingresa el motivo",
      type: 'warning',
      showCancelButton: true,
      confirmButtonColor: '#3085d6',
      cancelButtonColor: '#d33',
      confirmButtonText: 'SI',
      input: 'text',
      preConfirm: (descripcion) => {
        solicitud.desaprobar=descripcion;
        return true;
      },
      allowOutsideClick: () => !Swal.isLoading()
    }).then((result) => {
      if (result.value==true) {
        this.actualizarSolicitudDesaprobar(solicitud);
        this.ConsultaPrincipal(new Busqueda());
        
      }
    });
  }

  ConsultaPrincipal(busqueda: Busqueda) {
    this.listarSolicitud(busqueda);
  }

  listarTipoPrioridad() {
    this.servicioTipoPrioridad.listarTipoPrioridad().subscribe(
      data => {
        this.listaTipoPrioridad = data;
      }
    );
  }
  listarTipoSolicitud() {
    this.servicioTipoSolicitud.listarTipoSolicitud().subscribe(
      data => {
        this.listaTipoSolicitud = data;
      }
    );
  }

  listarEstado() {
    this.servicioEstado.listarEstado().subscribe(
      data => {
        this.listaEstado = data;
      }
    );
  }

  listarSolicitud(busqueda: Busqueda) {
    this.solicitudServicio.listarSolicitud(busqueda).subscribe(
      data => {
        this.listaSolicitudes = data.content as Solicitud[];
        this.totalfilasPorPagina = this.listaSolicitudes.length;
        this.totalRegistros=data.totalElements;
      });
  }

  LimpiarControles() {
    this.busqueda = new Busqueda();
  }

  cambiarPagina(pagina) {
    this.paginaActual = pagina.page;
    this.numeroPagina = this.numPaginasMostrar;
    this.busqueda.skip=this.paginaActual;
    this.ConsultaPrincipal(this.busqueda);
  }

  diferenciaFechas(fechaInicio, fechaFin) {
    const f1: Date = new Date(fechaInicio);
    const f2: Date = new Date(fechaFin);
    const resultado = ((f2.getTime() - f1.getTime()) / 1000 / 60 / 60);
    return resultado.toFixed(2);
  }

  formatDate(date) {
    var d = new Date(date),
      month = '' + (d.getMonth() + 1),
      day = '' + d.getDate(),
      year = d.getFullYear();

    if (month.length < 2)
      month = '0' + month;
    if (day.length < 2)
      day = '0' + day;

    if (!year)
      return '19000101';
    else
      return [year, month, day].join('');
  }
}
