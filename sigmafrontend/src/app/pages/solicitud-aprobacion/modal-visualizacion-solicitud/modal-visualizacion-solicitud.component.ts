import { Component, OnInit, Output, EventEmitter } from '@angular/core';

import { BsModalRef } from 'ngx-bootstrap/modal';
import { SolicitudService } from 'src/app/_service/solicitud.service';
import { Funciones } from 'src/app/_shared/funciones';
import Swal from 'sweetalert2';
import { Solicitud } from 'src/app/_model/solicitud';
import { SolicitudRecurso } from 'src/app/_model/solicitud-recurso';
import { SolicitudTecnico } from 'src/app/_model/solicitud-tecnico';
import { Tecnico } from 'src/app/_model/tecnico';
import { TipoSolicitudService } from 'src/app/_service/tipo-solicitud.service';
import { RecursoService } from 'src/app/_service/recurso.service';
import { TecnicoService } from 'src/app/_service/tecnico.service';

@Component({
  selector: 'app-modal-visualizacion-solicitud',
  templateUrl: './modal-visualizacion-solicitud.component.html',
  styleUrls: ['./modal-visualizacion-solicitud.component.scss']
})
export class ModalVisualizacionSolicitudComponent implements OnInit {

  cu_solicitud: Solicitud;
  listaTipoSolicitud: any = [];
  listaSubTipoSolicitud: any= [];
  listaRecursos: any[];
  listaTecnicos: any[];
  solicitudRecursos: any[]=[];
  solicitudTecnicos: any[]=[];

  solicitud_actualizada: Solicitud;


  cu_solicitudRecurso= new SolicitudRecurso();
  cu_solicitudTecnico= new SolicitudTecnico();

  cu_tecnico= new Tecnico();

  cantidad_recursos=0;
  cantidad_tecnicos=0;

  @Output()
  retorno = new EventEmitter();
  bEstado = false;
  constructor(
    private modalRef: BsModalRef,
    private solicitudService: SolicitudService,
    private tipoSolicitudService: TipoSolicitudService,
    private recursoService: RecursoService,
    private tecnicoService: TecnicoService,
    public funciones: Funciones
  ) { }

  ngOnInit() {
    this.listarTipoSolicitud();
    this.listarSubTipoSolicitud(this.cu_solicitud.tipo_solicitud.id_tipo_solicitud);
    this.cantidad_recursos=0;
    this.cantidad_tecnicos=0;
  }

  listarTipoSolicitud() {
    this.tipoSolicitudService.listarTipoSolicitud().subscribe(
      data => {
        this.listaTipoSolicitud = data;
      }
    );
  }

  listarSubTipoSolicitud(id_sub_tipo_solicitud) {
    this.tipoSolicitudService.listarSubTipoSolicitud(id_sub_tipo_solicitud).subscribe(
      data => {
        this.listaSubTipoSolicitud = data.subTiposSolicitud;
      }
    );
  }

  closeModal() {
    this.retorno.emit('0');
    this.modalRef.hide();
  }
}
