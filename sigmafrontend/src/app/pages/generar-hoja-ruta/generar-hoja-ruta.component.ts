import { Component, OnInit } from '@angular/core';
import { Programacion } from 'src/app/_model/programacion';
import { BsModalRef, BsModalService } from 'ngx-bootstrap/modal';
import { ProgramacionService } from 'src/app/_service/programacion.service';
import { ModalGenerarHojaRutaComponent } from './modal-generar-hoja-ruta/modal-generar-hoja-ruta.component';

@Component({
  selector: 'app-generar-hoja-ruta',
  templateUrl: './generar-hoja-ruta.component.html',
  styleUrls: ['./generar-hoja-ruta.component.scss']
})
export class GenerarHojaRutaComponent implements OnInit {

  listaHojaRuta: any = [];
  listaVisitasProgramadas: any = [];
  totalfilasPorPagina = 0;
  paginaActiva = 0;
  numeroPagina = 0;
  numFilas = 10;
  numPaginasMostrar = 5;
  totalRegistros = 0;
  fechaInicio: Date;
  fechaFin: Date;


  config;
  bsModal: BsModalRef;

  constructor(
    private modalService: BsModalService,
    private programacionService: ProgramacionService) { }

  ngOnInit() {
    this.listarHojaRuta();
  }

  listarHojaRuta() {
    this.programacionService.listarProgramacion().subscribe(
      data => {
        console.log(data);

        this.listaVisitasProgramadas = data;
      }

    )

  }

  openModalMapa(programacion: Programacion) {

    this.config = {
      ignoreBackdropClick: true,
      keyboard: false,
      animated: true,
      class: 'modal-extra',
      initialState: {
        listaHojaRuta: programacion
      }
    };

    this.bsModal = this.modalService.show(ModalGenerarHojaRutaComponent, this.config);
  }

  LimpiarControles() {

  }

  BuscarHojaRuta() {

  }
  cambiarPagina(pagina) {
    this.paginaActiva = ((pagina.page - 1) * this.numPaginasMostrar);
    this.numeroPagina = this.paginaActiva;
    this.listarHojaRuta();
  }

}
