import { Component, OnInit } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Router } from '@angular/router';
import { BsModalRef } from 'ngx-bootstrap/modal';
import 'leaflet';
import 'leaflet-routing-machine';
import { timeout } from 'q';


declare let L;

@Component({
  selector: 'app-modal-generar-hoja-ruta',
  templateUrl: './modal-generar-hoja-ruta.component.html',
  styleUrls: ['./modal-generar-hoja-ruta.component.scss']
})
export class ModalGenerarHojaRutaComponent implements OnInit {

  map: any;
  DataShape: any;
  Lgeojson: any;
  MapaJson: any;
  DepartamentoActual = '00';
  NombreDepartamento = '';

  listaHojaRuta:any;

  constructor(private http: HttpClient, private router: Router, private modalRef: BsModalRef) { }

  ngOnInit() {
    setTimeout(() => {
      this.generarmapa();
    }, 500);

  }

  generarmapa() {
    this.map = L.map('map').setView([-12.0700991, -77.0422714], 12);

    L.tileLayer('https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png', {
      attribution: '© <a href="https://www.openstreetmap.org/copyright">OpenStreetMap</a> contributors'
    }).addTo(this.map);

    L.Routing.control({
      waypoints: [
        L.latLng(-12.0672499, -77.0471004),
        L.latLng(-12.0662819, -77.0464933)
      ]
    }).addTo(this.map);

  }

  CargarShapeDB() {
    const path = './assets/mapa/00.json';
    this.http.get(path).subscribe((data: any) => {
      this.MapaJson = data;
      this.DataShape = JSON.parse(JSON.stringify(this.MapaJson));
      this.AddGeojson();

    });
  }

  Procesar(CodDepartamentoActual?: any) {
    if (CodDepartamentoActual !== undefined) {
      this.DepartamentoActual = CodDepartamentoActual;
    }

    // tslint:disable-next-line: triple-equals
    if (this.DepartamentoActual === '00') {
      this.DataShape = JSON.parse(JSON.stringify(this.MapaJson));
      this.AddGeojson();
    }
    if (this.DepartamentoActual !== '00') {
      let shapeJson;
      shapeJson = this.MapaJson.features.filter(x => x.properties.iddpto === this.DepartamentoActual);
      this.NombreDepartamento = this.MapaJson.features.filter(x => x.properties.iddpto === this.DepartamentoActual)[0].properties.nombdep;
      this.DataShape = JSON.parse(JSON.stringify(shapeJson));
      this.AddGeojson();
    }
  }

  AddGeojson() {
    const principal = this;
    this.RemoveGeojson();
    this.Lgeojson = L.geoJSON(principal.DataShape, {
      onEachFeature(feature, layer) {

        layer.myTag = 'myGeoJSON';
        // tslint:disable-next-line: only-arrow-functions
        layer.on('click', function(e) {
          if (principal.DepartamentoActual === '00') {
            const info = e.target.feature.properties;
            principal.NombreDepartamento = info.nombdep;
            principal.DepartamentoActual = info.iddpto;
            principal.Procesar();

          }


        });
        // tslint:disable-next-line: only-arrow-functions
        layer.on('mouseover', function(e) {
          // layer.bindPopup("dasdas", { sticky: true });
          // layer.bindTooltip(e.target.feature.properties.nombdep, { sticky: true });
          // layer.bindTooltip(principal.DetalleHover(e.target.feature.properties.iddpto)).openTooltip();
        });
      }
    });
    this.Lgeojson.addTo(this.map);
    this.map.fitBounds(this.Lgeojson.getBounds());

  }

  CargarShape(event?: any, CodDepartamentoActual?: string) {

    let shapeJson;
    if (this.DepartamentoActual !== '00') {
      shapeJson = this.MapaJson.features.filter(x => x.properties.iddpto === this.DepartamentoActual);
    } else {
      event.preventDefault();
      shapeJson = JSON.parse(JSON.stringify(this.MapaJson));
      this.DepartamentoActual = '00';
      this.NombreDepartamento = '';
    }
    this.DataShape = JSON.parse(JSON.stringify(shapeJson));
    this.AddGeojson();
  }

  RemoveGeojson() {
    const map = this.map;
    // tslint:disable-next-line: only-arrow-functions
    map.eachLayer(function(layer) {
      if (layer.hasOwnProperty('myTag')) {
        if (layer.myTag && layer.myTag === 'myGeoJSON') {
          map.removeLayer(layer);
        }
      }
    });
  }

  closeModal() {
    this.modalRef.hide();
  }


}
