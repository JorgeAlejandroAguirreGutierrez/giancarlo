import { Component, OnInit, Output, EventEmitter } from '@angular/core';

import { BsModalRef } from 'ngx-bootstrap/modal';
import { OrdenServicioService } from 'src/app/_service/orden-servicio.service';
import { Funciones } from 'src/app/_shared/funciones';
import Swal from 'sweetalert2';
import { OrdenServicio } from 'src/app/_model/orden-servicio';
import { Tecnico } from 'src/app/_model/tecnico';
import { TipoSolicitudService } from 'src/app/_service/tipo-solicitud.service';
import { TipoPrioridadService } from 'src/app/_service/tipo-prioridad.service';
import { ComplejidadServicioService } from 'src/app/_service/complejidad-servicio.service';
import { OrdenServicioRecurso } from 'src/app/_model/orden-servicio-recurso';
import { OrdenServicioTecnico } from 'src/app/_model/orden-servicio-tecnico';
import { ComplejidadServicio } from 'src/app/_model/complejidad-servicio';
import { ParametroSistemaService } from 'src/app/_service/parametro-sistema.service';
import { ParametroSistema } from 'src/app/_model/parametro-sistema';

@Component({
  selector: 'app-modal-edicion-orden',
  templateUrl: './modal-edicion-orden-servicio.component.html',
  styleUrls: ['./modal-edicion-orden-servicio.component.scss']
})
export class ModalEdicionOrdenServicioComponent implements OnInit {

  cu_ordenServicio: OrdenServicio;
  listaTipoSolicitud: any = [];
  listaSubTipoSolicitud: any = [];
  listaTipoPrioridad: any = [];
  listaComplejidadServicio: any = [];
  listaRecursos: any[];
  listaTecnicos: any[];
  listaParametros: any[];
  ordenServicioRecursos: any[] = [];
  ordenServicioTecnicos: any[] = [];

  ordenServicio_actualizada: OrdenServicio;


  cu_ordenServicioRecurso = new OrdenServicioRecurso();
  cu_ordenServicioTecnico = new OrdenServicioTecnico();

  cu_tecnico = new Tecnico();

  listaParametrosRecursos: ParametroSistema[]=[];
  listaParametrosTecnicos: ParametroSistema[]=[];

  @Output()
  retorno = new EventEmitter();
  bEstado = false;
  constructor(
    private modalRef: BsModalRef,
    private ordenServicioService: OrdenServicioService,
    private tipoSolicitudService: TipoSolicitudService,
    private tipoPrioridadService: TipoPrioridadService,
    private complejidadServicioService: ComplejidadServicioService,
    private parametroSistemaService: ParametroSistemaService,
    public funciones: Funciones
  ) { }

  ngOnInit() {
    this.listarTipoSolicitud();
    this.listarSubTipoSolicitud();
    this.listarTipoPrioridad();
    this.listarComplejidadServicio();
    this.listarParametrosTipo();
    this.eliminarRepetidos();
  }

  eliminarRepetidos() {
    for (let i=0; i<this.cu_ordenServicio.solicitud.parametroSistema.length; i++) {
      let bandera=false;
        for (let j=0; j<this.listaParametrosRecursos.length; j++) {
          if (this.listaParametrosRecursos[j].recurso==this.cu_ordenServicio.solicitud.parametroSistema[i].recurso) {
            bandera=true;
          }
        }
        if (!bandera) {
          this.listaParametrosRecursos.push(this.cu_ordenServicio.solicitud.parametroSistema[i]);
        }
    }
    for (let i=0; i<this.cu_ordenServicio.solicitud.parametroSistema.length; i++) {
      let bandera=false;
        for (let j=0; j<this.listaParametrosTecnicos.length; j++) {
          if (this.listaParametrosTecnicos[j].especialidad==this.cu_ordenServicio.solicitud.parametroSistema[i].especialidad) {
            bandera=true;
          }
        }
        if (!bandera) {
          this.listaParametrosTecnicos.push(this.cu_ordenServicio.solicitud.parametroSistema[i]);
        }
    }
  }

  guardarOrdenServicio() {
    this.actualizarOrdenServicio();
  }

  actualizarOrdenServicio() {
    this.ordenServicioService.actualizarOrdenServicio(this.cu_ordenServicio).subscribe(
      data => {
        this.ordenServicio_actualizada = data as OrdenServicio;
        Swal.fire('Exito', 'Se ha actualizado correctamente la orden de servicio', 'success');
        this.closeModal();
      }
    );
  }

  listarTipoSolicitud() {
    this.tipoSolicitudService.listarTipoSolicitud().subscribe(
      data => {
        this.listaTipoSolicitud = data;
      }
    );
  }

  listarSubTipoSolicitud() {
    let id_sub_tipo_solicitud=this.cu_ordenServicio.solicitud.tipo_solicitud.id_tipo_solicitud;
    this.tipoSolicitudService.listarSubTipoSolicitud(id_sub_tipo_solicitud).subscribe(
      data => {
        this.listaSubTipoSolicitud = data.subTiposSolicitud;
      }
    );
  }

  listarTipoPrioridad() {
    this.tipoPrioridadService.listarTipoPrioridad().subscribe(
      data => {
        this.listaTipoPrioridad = data;
      }
    );
  }

  listarComplejidadServicio() {
    this.complejidadServicioService.listarComplejidadServicio().subscribe(
      data => {
        this.listaComplejidadServicio = data;
      }
    );
  }

  listarParametrosTipo() {
    let id_tipo_solicitud = this.cu_ordenServicio.solicitud.tipo_solicitud.id_tipo_solicitud;
    let id_sub_tipo_solicitud = this.cu_ordenServicio.solicitud.sub_tipo_solicitud.id_sub_tipo_solicitud;
    this.parametroSistemaService.listarParametrosTipo(id_tipo_solicitud, id_sub_tipo_solicitud).subscribe(
      data => {
        this.listaParametros = data;
      }
    );
  }

  closeModal() {
    this.retorno.emit('0');
    this.modalRef.hide();
  }
}
