import { Component, OnInit } from '@angular/core';
import { BsModalRef, BsModalService } from 'ngx-bootstrap/modal';
import { Busqueda } from 'src/app/_model/busqueda';
import { ModalOrdenServicioComponent } from './modal-orden-servicio/modal-orden-servicio.component';
import { ModalEdicionOrdenServicioComponent } from './modal-edicion-orden-servicio/modal-edicion-orden-servicio.component';
import Swal from 'sweetalert2';
import { OrdenServicio } from 'src/app/_model/orden-servicio';
import { OrdenServicioService } from 'src/app/_service/orden-servicio.service';
import { ParametroSistemaService } from 'src/app/_service/parametro-sistema.service';

@Component({
  selector: 'app-orden-servicio',
  templateUrl: './orden-servicio.component.html',
  styleUrls: ['./orden-servicio.component.scss']
})
export class OrdenServicioComponent implements OnInit {

  lista_ordenes_servicio: any = [];
  lista_parametros_orden_servicio: any[] = [];

  numOrdenMask = ['O', 'R', 'D', '-', /[0-9]/, /\d/, /\d/, /\d/, /\d/, /\d/];

  totalRegistros = 0;
  totalfilasPorPagina = 0;
  paginaActiva = 0;
  numeroPagina = 5;
  numFilas = 5;
  numPaginasMostrar = 5;
  paginaActual: number = 0;

  config;
  bsModal: BsModalRef;
  busqueda: Busqueda;

  constructor(
    private ordenService: OrdenServicioService,
    private modalService: BsModalService,
  ) { }

  ngOnInit() {
    this.busqueda = new Busqueda();
    this.ConsultaPrincipal(this.busqueda);

  }

  crearOrdenServicio() {
    this.openModal();
  }

  openModal() {
    this.config = {
      //ignoreBackdropClick: true,
      //keyboard: false,
      animated: true,
      class: 'modal-extra'
    };

    this.bsModal = this.modalService.show(ModalOrdenServicioComponent, this.config);
    this.bsModal.content.retorno.subscribe(
      data => {
        this.ConsultaPrincipal(new Busqueda());
      }
    );
  }

  ConsultaPrincipal(busqueda: Busqueda) {
    this.listarOrden(busqueda);
  }

  listarOrden(busqueda: Busqueda) {
    this.ordenService.listarOrdenServicio(busqueda).subscribe(
      data => {
        this.lista_ordenes_servicio = data.content as OrdenServicio[];
        this.totalfilasPorPagina = this.lista_ordenes_servicio.length;
        this.totalRegistros = data.totalElements;
      });
  }

  LimpiarControles() {
    this.busqueda = new Busqueda();

  }

  cambiarPagina(pagina) {
    this.paginaActual = pagina.page;
    this.numeroPagina = this.numPaginasMostrar;
    this.busqueda.skip = this.paginaActual;
    this.ConsultaPrincipal(this.busqueda);
  }

  openModalEdicion(ordenServicio: OrdenServicio) {
    this.config = {
      animated: true,
      class: 'modal-extra',
      initialState: {
        cu_ordenServicio: ordenServicio,
      }
    };
    this.bsModal = this.modalService.show(ModalEdicionOrdenServicioComponent, this.config);
    this.bsModal.content.retorno.subscribe(
      data => {
        this.ConsultaPrincipal(new Busqueda());
      }
    );
  }

  diferenciaFechas(fechaInicio, fechaFin) {
    const f1: Date = new Date(fechaInicio);
    const f2: Date = new Date(fechaFin);
    const resultado = ((f2.getTime() - f1.getTime()) / 1000 / 60 / 60);
    return resultado.toFixed(2);
  }

  formatDate(date) {
    var d = new Date(date),
      month = '' + (d.getMonth() + 1),
      day = '' + d.getDate(),
      year = d.getFullYear();

    if (month.length < 2)
      month = '0' + month;
    if (day.length < 2)
      day = '0' + day;


    if (!year)
      return '19000101';
    else
      return [year, month, day].join('');
  }

  eliminar(orden: OrdenServicio) {
    Swal.fire({
      title: 'Eliminar',
      text: "Deseas eliminar la solicitud",
      type: 'warning',
      showCancelButton: true,
      confirmButtonColor: '#3085d6',
      cancelButtonColor: '#d33',
      confirmButtonText: 'SI'
    }).then((result) => {
      console.log(confirm);
      if (result.value == true) {
        this.eliminarOrden(orden);
      }
    });
  }

  eliminarOrden(orden: OrdenServicio) {
    this.ordenService.eliminarOrdenServicio(orden.id_orden_servicio).subscribe(
      data => {
        this.ConsultaPrincipal(new Busqueda());
        Swal.fire(
          'Exito',
          'Se ha eliminado la solicitud correctamente.',
          'success'
        );
      }
    );
  }
}
