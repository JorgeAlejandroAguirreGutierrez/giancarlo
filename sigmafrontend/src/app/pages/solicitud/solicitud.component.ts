import { Component, OnInit } from '@angular/core';
import { BsModalRef, BsModalService } from 'ngx-bootstrap/modal';
import { Busqueda } from 'src/app/_model/busqueda';
import { Solicitud } from 'src/app/_model/solicitud';
import { SolicitudService } from 'src/app/_service/solicitud.service';
import { TipoPrioridadService } from 'src/app/_service/tipo-prioridad.service';
import { TipoSolicitudService } from 'src/app/_service/tipo-solicitud.service';
import { EstadoService } from 'src/app/_service/estado.service';
import { ModalSolicitudComponent } from './modal-solicitud/modal-solicitud.component';
import { ModalEdicionSolicitudComponent } from './modal-edicion-solicitud/modal-edicion-solicitud.component';
import Swal from 'sweetalert2';

@Component({
  selector: 'app-solicitud',
  templateUrl: './solicitud.component.html',
  styleUrls: ['./solicitud.component.scss']
})
export class SolicitudComponent implements OnInit {

  listaSolicitudes: any = [];
  listaEstadoSolicitud: any = [];
  listaTipoSolicitud: any = [];
  listaTipoPrioridad: any = [];
  listaParametroSistema: any = [];

  numOrdenMask = ['S', 'O', 'L', '-', /[0-9]/, /\d/, /\d/, /\d/, /\d/, /\d/];

  totalRegistros = 0;
  totalfilasPorPagina = 0;
  paginaActiva = 0;
  numeroPagina = 5;
  numFilas = 5;
  numPaginasMostrar = 5;
  paginaActual: number = 0;

  config;
  bsModal: BsModalRef;
  busqueda: Busqueda;

  constructor(
    private solicitudServicio: SolicitudService,
    private modalService: BsModalService,
    private servicioTipoPrioridad: TipoPrioridadService,
    private servicioTipoSolicitud: TipoSolicitudService,
    private servicioEstado: EstadoService,
  ) { }

  ngOnInit() {
    this.busqueda = new Busqueda();
    this.ConsultaPrincipal(this.busqueda);
    this.listarTipoSolicitud();
    this.listarEstadoSolicitud();

  }

  ConsultaPrincipal(busqueda: Busqueda) {
    this.listarSolicitud(busqueda);
  }

  listarTipoPrioridad() {
    this.servicioTipoPrioridad.listarTipoPrioridad().subscribe(
      data => {
        this.listaTipoPrioridad = data;
      }
    );
  }
  listarTipoSolicitud() {
    this.servicioTipoSolicitud.listarTipoSolicitud().subscribe(
      data => {
        this.listaTipoSolicitud = data;
      }
    );
  }

  listarEstadoSolicitud() {
    this.servicioEstado.listarEstado().subscribe(
      data => {
        this.listaEstadoSolicitud = data;
      }
    );
  }

  listarSolicitud(busqueda: Busqueda) {
    this.solicitudServicio.listarSolicitud(busqueda).subscribe(
      data => {
        this.listaSolicitudes = data.content as Solicitud[];
        this.totalfilasPorPagina = this.listaSolicitudes.length;
        this.totalRegistros=data.totalElements;
      });

  }

  LimpiarControles() {
    this.busqueda = new Busqueda();

  }

  cambiarPagina(pagina) {
    this.paginaActual = pagina.page;
    this.numeroPagina = this.numPaginasMostrar;
    this.busqueda.skip=this.paginaActual;
    console.log('pagina actual'+ this.paginaActual);
    console.log('paginaActiva'+ this.paginaActiva);
    console.log('numeroPagina'+ this.numeroPagina);
    this.ConsultaPrincipal(this.busqueda);
  }
  crearSolicitudServicio() {
    this.openModal();
  }

  openModal() {
    this.config = {
      //ignoreBackdropClick: true,
      //keyboard: false,
      animated: true,
      class: 'modal-extra'
    };

    this.bsModal = this.modalService.show(ModalSolicitudComponent, this.config);
    this.bsModal.content.retorno.subscribe(
      data => {
        this.ConsultaPrincipal(new Busqueda());
      }
    );
  }

  openModalEdicion(solicitud: Solicitud) {
    this.config = {
      //ignoreBackdropClick: true,
      //keyboard: false,
      animated: true,
      class: 'modal-extra',
      initialState: {
        cu_solicitud: solicitud,
      }
    };
    this.bsModal = this.modalService.show(ModalEdicionSolicitudComponent, this.config);
    this.bsModal.content.retorno.subscribe(
      data => {
        this.ConsultaPrincipal(new Busqueda());
      }
    );
  }

  diferenciaFechas(fechaInicio, fechaFin) {
    const f1: Date = new Date(fechaInicio);
    const f2: Date = new Date(fechaFin);
    const resultado = ((f2.getTime() - f1.getTime()) / 1000 / 60 / 60);
    return resultado.toFixed(2);
  }

  formatDate(date) {
    var d = new Date(date),
      month = '' + (d.getMonth() + 1),
      day = '' + d.getDate(),
      year = d.getFullYear();

    if (month.length < 2)
      month = '0' + month;
    if (day.length < 2)
      day = '0' + day;


    if (!year)
      return '19000101';
    else
      return [year, month, day].join('');
  }

  eliminar(solicitud: Solicitud) {
    Swal.fire({
      title: 'Eliminar',
      text: "Deseas eliminar la solicitud",
      type: 'warning',
      showCancelButton: true,
      confirmButtonColor: '#3085d6',
      cancelButtonColor: '#d33',
      confirmButtonText: 'SI'
    }).then((result) => {
      console.log(confirm);
      if (result.value==true) {
        this.eliminarSolicitud(solicitud);
      }
    });
  }

  eliminarSolicitud(solicitud: Solicitud) {
    this.solicitudServicio.eliminarSolicitud(solicitud).subscribe(
      data => {
        this.ConsultaPrincipal(new Busqueda());
        Swal.fire(
          'Exito',
          'Se ha eliminado la solicitud correctamente.',
          'success'
        );
      }
    );
  }
}
