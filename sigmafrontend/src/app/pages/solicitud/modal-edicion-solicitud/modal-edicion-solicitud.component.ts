import { Component, OnInit, Output, EventEmitter } from '@angular/core';

import { BsModalRef } from 'ngx-bootstrap/modal';
import { SolicitudService } from 'src/app/_service/solicitud.service';
import { Funciones } from 'src/app/_shared/funciones';
import Swal from 'sweetalert2';
import { Solicitud } from 'src/app/_model/solicitud';
import { TipoSolicitudService } from 'src/app/_service/tipo-solicitud.service';
import { ZonaService } from 'src/app/_service/zona.service';
import { ParametroSistema } from 'src/app/_model/parametro-sistema';

@Component({
  selector: 'app-modal-edicion-solicitud',
  templateUrl: './modal-edicion-solicitud.component.html',
  styleUrls: ['./modal-edicion-solicitud.component.scss']
})
export class ModalEdicionSolicitudComponent implements OnInit {

  cu_solicitud: Solicitud;
  lista_tipo_solicitud: any = [];
  lista_sub_tipo_solicitud: any= [];
  lista_zona: any= [];
  solicitud_actualizada: Solicitud;


  @Output()
  retorno = new EventEmitter();
  bEstado = false;
  constructor(
    private modalRef: BsModalRef,
    private solicitudService: SolicitudService,
    private tipoSolicitudService: TipoSolicitudService,
    private zonaService: ZonaService,
    public funciones: Funciones
  ) { }

  ngOnInit() {
    this.listarTipoSolicitud();
    this.listarSubTipoSolicitud(this.cu_solicitud.tipo_solicitud.id_tipo_solicitud);
    this.listarZona();
  }

  guardarSolicitud() {
    this.actualizarSolicitud();
  }

  actualizarSolicitud() {
    this.cu_solicitud.estado.id_estado=1;
    this.solicitudService.actualizarSolicitud(this.cu_solicitud).subscribe(
      data => {
        this.solicitud_actualizada = data as Solicitud;
        Swal.fire('Exito', 'Se ha actualizado exitosamente la solicitud', 'success');
        this.closeModal();
      },
      (err) => {
        Swal.fire('Error', 'No se pudo actualizar la solicitud', 'error');
      }
    );
  }

  listarTipoSolicitud() {
    this.tipoSolicitudService.listarTipoSolicitud().subscribe(
      data => {
        this.lista_tipo_solicitud = data;
      }
    );
  }

  listarSubTipoSolicitud(id_sub_tipo_solicitud) {
    this.tipoSolicitudService.listarSubTipoSolicitud(id_sub_tipo_solicitud).subscribe(
      data => {
        this.lista_sub_tipo_solicitud = data.subTiposSolicitud;
      }
    );
  }

  listarZona() {
    this.zonaService.listarZona().subscribe(
      data => {
        this.lista_zona = data;
      }
    );
  }

  closeModal() {
    this.retorno.emit('0');
    this.modalRef.hide();
  }
}
