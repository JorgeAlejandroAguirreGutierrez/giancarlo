import { Component, OnInit, Output, EventEmitter } from '@angular/core';
import { FormBuilder, FormGroup, Validators, FormControl } from '@angular/forms';
import { BsModalRef } from 'ngx-bootstrap/modal';
import { SolicitudService } from 'src/app/_service/solicitud.service';
import { TipoSolicitudService } from 'src/app/_service/tipo-solicitud.service';
import { TipoPrioridadService } from 'src/app/_service/tipo-prioridad.service';
import { ZonaService } from 'src/app/_service/zona.service';
import { Solicitud } from 'src/app/_model/solicitud';
import { SolicitudRecurso } from 'src/app/_model/solicitud-recurso';
import { SolicitudTecnico } from 'src/app/_model/solicitud-tecnico';
import { Tecnico } from 'src/app/_model/tecnico';
import { Funciones } from 'src/app/_shared/funciones';
import Swal from 'sweetalert2';
import { ParametroSistemaService } from 'src/app/_service/parametro-sistema.service';
import { ParametroSistema } from 'src/app/_model/parametro-sistema';

@Component({
  selector: 'app-modal-solicitud',
  templateUrl: './modal-solicitud.component.html',
  styleUrls: ['./modal-solicitud.component.scss']
})
export class ModalSolicitudComponent implements OnInit {

  listaParametroSistema: any = [];
  lista_tipo_solicitud: any = [];
  lista_sub_tipo_solicitud: any = [];
  lista_tipo_prioridad: any[];
  lista_zona: any[];
  listaRecursos: any[];
  listaTecnicos: any[];
  solicitudRecursos: any[] = [];
  solicitudTecnicos: any[] = [];

  cu_solicitud = new Solicitud();

  solicitud_creada: Solicitud;


  cu_solicitudRecurso = new SolicitudRecurso();
  cu_solicitudTecnico = new SolicitudTecnico();

  cu_tecnico = new Tecnico();

  cantidad_recursos = 0;
  cantidad_tecnicos = 0;

  @Output()
  retorno = new EventEmitter();
  bEstado = false;
  constructor(
    private formBuilder: FormBuilder,
    private modalRef: BsModalRef,
    private solicitudService: SolicitudService,
    private tipoSolicitudService: TipoSolicitudService,
    private tipoPrioridadService: TipoPrioridadService,
    private parametroSistemaService: ParametroSistemaService,
    private zonaService: ZonaService,
    public funciones: Funciones
  ) { }

  ngOnInit() {
    this.listarTipoSolicitud();
    this.listarTipoPrioridad();
    this.listarZona();
  }

  guardarSolicitud() {
    Swal.fire({
      title: 'Crear',
      text: "Deseas crear la solicitud",
      type: 'warning',
      showCancelButton: true,
      confirmButtonColor: '#3085d6',
      cancelButtonColor: '#d33',
      confirmButtonText: 'SI'
    }).then((result) => {
      if (result.value == true) {
        this.crearSolicitud();
      }
    });
  }

  crearSolicitud() {
    this.cu_solicitud.estado.id_estado = 1;
    let id_tipo_solicitud = this.cu_solicitud.tipo_solicitud.id_tipo_solicitud;
    let id_sub_tipo_solicitud = this.cu_solicitud.sub_tipo_solicitud.id_sub_tipo_solicitud;
    this.parametroSistemaService.listarParametrosTipo(id_tipo_solicitud, id_sub_tipo_solicitud).subscribe(
      data => {
        this.listaParametroSistema= data as ParametroSistema[];
        this.cu_solicitud.parametroSistema=this.listaParametroSistema;
        this.solicitudService.crearSolicitud(this.cu_solicitud).subscribe(
          data => {
            this.solicitud_creada = data as Solicitud;
            Swal.fire('Exito', 'Se ha creado exitosamente la solicitud', 'success');
            this.closeModal();
          },
          (err) => {
            Swal.fire('Error', 'No se pudo registrar la solicitud', 'error');
          }
        );
      },
      (err) => {
        Swal.fire('Error', 'No se pudo obtener los parametros', 'error');
      }
    );
    
  }

  listarTipoSolicitud() {
    this.tipoSolicitudService.listarTipoSolicitud().subscribe(
      data => {
        this.lista_tipo_solicitud = data;
      }
    );
  }

  listarSubTipoSolicitud(id_sub_tipo_solicitud) {
    this.tipoSolicitudService.listarSubTipoSolicitud(id_sub_tipo_solicitud).subscribe(
      data => {
        this.lista_sub_tipo_solicitud = data.subTiposSolicitud;
      }
    );
  }

  listarTipoPrioridad() {
    this.tipoPrioridadService.listarTipoPrioridad().subscribe(
      data => {
        this.lista_tipo_prioridad = data;
      }
    );
  }

  listarZona() {
    this.zonaService.listarZona().subscribe(
      data => {
        this.lista_zona = data;
      }
    );
  }

  closeModal() {
    this.retorno.emit('0');
    this.modalRef.hide();
  }
}
