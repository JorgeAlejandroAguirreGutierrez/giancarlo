import { Component, OnInit, Output, EventEmitter } from '@angular/core';

import { Tecnico } from 'src/app/_model/tecnico';
import { BsModalRef } from 'ngx-bootstrap/modal';
import { TecnicoService } from 'src/app/_service/tecnico.service';
import { ParametroSistemaService } from 'src/app/_service/parametro-sistema.service';
import { OrdenServicioService } from 'src/app/_service/orden-servicio.service';
import { Funciones } from 'src/app/_shared/funciones';
import { OrdenServicioTecnico } from 'src/app/_model/orden-servicio-tecnico';
import Swal from 'sweetalert2';
import { OrdenServicio } from 'src/app/_model/orden-servicio';
import { ParametroSistema } from 'src/app/_model/parametro-sistema';

@Component({
  selector: 'app-modal-asignar-tecnico',
  templateUrl: './modal-asignar-tecnico.component.html',
  styleUrls: ['./modal-asignar-tecnico.component.scss']
})
export class ModalAsignarTecnicoComponent implements OnInit {

  ordenServicio: OrdenServicio;
  listaTecnicos: any[];
  cu_ordenServicioTecnico = new OrdenServicioTecnico();
  listaParametrosTecnicos: ParametroSistema[]=[];

  id_tecnico: number;

  @Output()
  retorno = new EventEmitter();


  constructor(
    private tecnicoService: TecnicoService,
    private modalRef: BsModalRef,
    private ordenServicioService: OrdenServicioService,
    public funciones: Funciones
  ) { }

  ngOnInit() {
    this.listarTecnicos();
    this.eliminarRepetidos();
  }

  eliminarRepetidos() {
    for (let i=0; i<this.ordenServicio.solicitud.parametroSistema.length; i++) {
      let bandera=false;
        for (let j=0; j<this.listaParametrosTecnicos.length; j++) {
          if (this.listaParametrosTecnicos[j].especialidad==this.ordenServicio.solicitud.parametroSistema[i].especialidad) {
            bandera=true;
          }
        }
        if (!bandera) {
          this.listaParametrosTecnicos.push(this.ordenServicio.solicitud.parametroSistema[i]);
        }
    }
  }

  eliminar(i: number) {
    Swal.fire({
      title: 'Asignacion',
      text: "Deseas eliminar el tecnico",
      type: 'warning',
      showCancelButton: true,
      confirmButtonColor: '#3085d6',
      cancelButtonColor: '#d33',
      confirmButtonText: 'SI'
    }).then((result) => {
      if (result.value == true) {
        this.ordenServicio.ordenServicioTecnicos.splice(i, 1);
      }
    });

  }

  guardarAsignarTecnicos() {
    Swal.fire({
      title: 'Asignacion',
      text: "Deseas asignar tecnicos",
      type: 'warning',
      showCancelButton: true,
      confirmButtonColor: '#3085d6',
      cancelButtonColor: '#d33',
      confirmButtonText: 'SI'
    }).then((result) => {
      if (result.value == true) {
        this.actualizarOrdenServicio();
      }
    });
  }


  validarAsignacionTecnicos() {
    let bandera = 0
    this.ordenServicio.ordenServicioTecnicos.forEach(elemento1 => {
      this.ordenServicio.solicitud.parametroSistema.forEach(elemento2 => {
        if (elemento1.tecnico.especialidad == elemento2.especialidad) {
          bandera = bandera + 1;
        }
      });
    });
    return bandera;
  }

  actualizarOrdenServicio() {
    this.ordenServicio.estado.id_estado = 6;
    this.ordenServicioService.actualizarOrdenServicio(this.ordenServicio).subscribe(
      data => {
        Swal.fire('Exito', 'Se ha asignado exitosamente los tecnicos', 'success');
        this.closeModal();
      },
      (err) => {
        Swal.fire('Error', 'No se pudo actualizar la orden de servicio', 'error');
      }
    );
  }

  listarTecnicos() {
    this.tecnicoService.listarTecnicos().subscribe(
      data => {
        this.listaTecnicos = data as Tecnico[];
      }
    );
  }

  agregarTecnico() {
    let tecnico = this.obtenerTecnico(this.id_tecnico);
    let bandera = false;
    for (let i=0; i<this.ordenServicio.solicitud.parametroSistema.length; i++) {
      if (this.ordenServicio.solicitud.parametroSistema[i].especialidad == tecnico.especialidad) {
        this.cu_ordenServicioTecnico=new OrdenServicioTecnico();
        this.cu_ordenServicioTecnico.tecnico=tecnico;
        this.ordenServicio.ordenServicioTecnicos.push(this.cu_ordenServicioTecnico);
        bandera = true;
        break;
      }
    }
    if (!bandera) {
      Swal.fire('Error', 'Tecnico no asignable', 'error');
    }
    this.cu_ordenServicioTecnico=new OrdenServicioTecnico();
  }

  obtenerTecnico(id_tecnico: number) {
    for (let i = 0; i < this.listaTecnicos.length; i++) {
      if (this.listaTecnicos[i].id_tecnico == id_tecnico) {
        return this.listaTecnicos[i];
      }
    }
  }

  closeModal() {
    this.retorno.emit('0');
    this.modalRef.hide();
  }
}
