import { Component, OnInit } from '@angular/core';
import { BsModalRef, BsModalService } from 'ngx-bootstrap/modal';
import { Busqueda } from 'src/app/_model/busqueda';
import { OrdenServicioService } from 'src/app/_service/orden-servicio.service';
import { TipoPrioridadService } from 'src/app/_service/tipo-prioridad.service';
import { TipoSolicitudService } from 'src/app/_service/tipo-solicitud.service';
import { EstadoService } from 'src/app/_service/estado.service';
import Swal from 'sweetalert2';
import { OrdenServicio } from 'src/app/_model/orden-servicio';
import { ModalAsignarTecnicoComponent } from './modal-asignar-tecnico/modal-asignar-tecnico.component';

@Component({
  selector: 'app-asignar-tecnico-orden',
  templateUrl: './asignar-tecnico-orden.component.html',
  styleUrls: ['./asignar-tecnico-orden.component.scss']
})
export class AsignarTecnicoOrdenComponent implements OnInit {

  listaOrdenServicio: any = [];
  listaEstadoSolicitud: any = [];
  listaTipoSolicitud: any = [];
  listaTipoPrioridad: any = [];

  numOrdenMask = ['O', 'R', 'D', '-', /[0-9]/, /\d/, /\d/, /\d/, /\d/, /\d/];

  totalRegistros = 0;
  totalfilasPorPagina = 0;
  paginaActiva = 0;
  numeroPagina = 5;
  numFilas = 5;
  numPaginasMostrar = 5;
  paginaActual: number = 0;

  config;
  bsModal: BsModalRef;
  busqueda: Busqueda;

  constructor(
    private ordenServicio: OrdenServicioService,
    private modalService: BsModalService,
    private servicioTipoPrioridad: TipoPrioridadService,
    private servicioTipoSolicitud: TipoSolicitudService,
    private servicioEstado: EstadoService,
  ) { }

  ngOnInit() {
    this.busqueda = new Busqueda();
    this.ConsultaPrincipal(this.busqueda);
    this.listarTipoSolicitud();
    this.listarEstadoSolicitud();

  }

  ConsultaPrincipal(busqueda: Busqueda) {
    this.listarOrdenServicio(busqueda);
  }

  listarTipoPrioridad() {
    this.servicioTipoPrioridad.listarTipoPrioridad().subscribe(
      data => {
        this.listaTipoPrioridad = data;
      }
    );
  }
  listarTipoSolicitud() {
    this.servicioTipoSolicitud.listarTipoSolicitud().subscribe(
      data => {
        this.listaTipoSolicitud = data;
      }
    );
  }

  listarEstadoSolicitud() {
    this.servicioEstado.listarEstado().subscribe(
      data => {
        this.listaEstadoSolicitud = data;
      }
    );
  }

  listarOrdenServicio(busqueda: Busqueda) {
    this.ordenServicio.listarOrdenServicio(busqueda).subscribe(
      data => {
        this.listaOrdenServicio = data.content as OrdenServicio[];
        console.log(this.listaOrdenServicio);
        this.totalfilasPorPagina = this.listaOrdenServicio.length;
        this.totalRegistros=data.totalElements;
      });

  }

  LimpiarControles() {
    this.busqueda = new Busqueda();

  }

  cambiarPagina(pagina) {
    this.paginaActual = pagina.page;
    this.numeroPagina = this.numPaginasMostrar;
    this.busqueda.skip=this.paginaActual;
    this.ConsultaPrincipal(this.busqueda);
  }
  asignar(ordenServicio: OrdenServicio) {
    if (ordenServicio.estado.estado=="ORDEN DE SERVICIO PENDIENTE" || ordenServicio.estado.estado=="PENDIENTE ASIGNACION RECURSOS Y TECNICOS") {
      this.openModal(ordenServicio);
    } else {
      Swal.fire('Error', 'Los recursos aun no se pueden cargar', 'error');
    }
    
  }

  openModal(ordenServicio: OrdenServicio) {
    this.config = {
      //ignoreBackdropClick: true,
      //keyboard: false,
      animated: true,
      class: 'modal-extra',
      initialState: {
        ordenServicio: ordenServicio,
      }
    };

    this.bsModal = this.modalService.show(ModalAsignarTecnicoComponent, this.config);
    this.bsModal.content.retorno.subscribe(
      data => {
        this.ConsultaPrincipal(new Busqueda());
      }
    );
  }

  diferenciaFechas(fechaInicio, fechaFin) {
    const f1: Date = new Date(fechaInicio);
    const f2: Date = new Date(fechaFin);
    const resultado = ((f2.getTime() - f1.getTime()) / 1000 / 60 / 60);
    return resultado.toFixed(2);
  }

  formatDate(date) {
    var d = new Date(date),
      month = '' + (d.getMonth() + 1),
      day = '' + d.getDate(),
      year = d.getFullYear();
    if (month.length < 2)
      month = '0' + month;
    if (day.length < 2)
      day = '0' + day;
    if (!year)
      return '19000101';
    else
      return [year, month, day].join('');
  }
}
