import { TipoSolicitud } from './tipo-solicitud';

export class SubTipoSolicitud {
    id_sub_tipo_solicitud: number;
    sub_tipo_solicitud: string;
    activo: boolean;
    tipo_solicitud: TipoSolicitud;
}