import { OrdenServicio } from './orden-servicio';
import { Recurso } from './recurso';
export class OrdenServicioRecurso {
  public id_orden_servicio_recurso: number;
  public ordenServicio: OrdenServicio;
  public recurso: Recurso;


  constructor() {
    this.id_orden_servicio_recurso=0;
    this.ordenServicio=new OrdenServicio();
    this.recurso=new Recurso();
  }
}
