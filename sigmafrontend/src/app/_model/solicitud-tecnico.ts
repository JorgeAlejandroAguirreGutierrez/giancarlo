import { Solicitud } from './solicitud';
import { Tecnico } from './tecnico';

export class SolicitudTecnico {
    public id: number;
    public solicitud: Solicitud;
    public tecnico: Tecnico;

    constructor(){
      this.tecnico=new Tecnico();
      this.solicitud=new Solicitud();
    }

}
