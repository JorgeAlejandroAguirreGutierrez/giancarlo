export class Recurso {
  public idRecurso: number;
  public cod_recurso: string;
  public nombre: string;
  public cantidad: number;
  public activo: boolean;
  public unidad_medida: string;
  public usuario_creacion: number;
  public fecha_creacion: Date;
  public usuario_modificacion: string;
  public fecha_modificacion: Date;
  public usuario_eliminacion: string;
  public fecha_eliminacion: Date;
}
