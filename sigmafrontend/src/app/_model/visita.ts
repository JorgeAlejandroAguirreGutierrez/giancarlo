export class Visita {
    public id_visita: number;
    public codigo_visita: string;
    public descripcion: string;
    public observacion: string;
    public fecha_visita: string;
    public activo: Boolean;
    public fecha_creacion: Date;
    public fecha_eliminacion: Date;
    public fecha_modificacion: Date;
    public usuario_creacion: number;
    public usuario_eliminacion: number;
    public usuario_modificacion: number;
}