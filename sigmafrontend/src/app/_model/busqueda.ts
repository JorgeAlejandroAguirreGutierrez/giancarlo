export class Busqueda {

    public id_orden: number = 0;
    public numero_orden: string = null;
    public numero_solicitud: string = null;
    public fecha_recepcion: Date;
    public id_tipo_solicitud: number = 0;
    public id_estado: number = 0;
    public skip: number = 0;
    public take: number = 10;
}