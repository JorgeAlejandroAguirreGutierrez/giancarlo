export class TipoPrioridad {
    id_tipo_prioridad: number;
    tipo_prioridad: string;
    activo: boolean;
}