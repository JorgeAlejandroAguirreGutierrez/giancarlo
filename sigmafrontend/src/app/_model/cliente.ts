import { TipoCliente } from './TipoCLiente';
import { Zona } from './zona';

export class Cliente {
  id_cliente: number;
  cliente: string;
  representante: string;
  direccion: string;
  telefono: string;
  ubigeo: string;
  cod_postal: string;
  ubicacion: string;
  activo: boolean;
  tipo_cliente: TipoCliente;
  zona: Zona;

  constructor() {
    this.tipo_cliente=new TipoCliente();
    this.zona=new Zona();
  }

}