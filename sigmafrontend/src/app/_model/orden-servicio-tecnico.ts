import { Tecnico } from './tecnico';
import { OrdenServicio } from './orden-servicio';
export class OrdenServicioTecnico {
  public id_orden_servicio_tecnico: number
  public ordenServicio: OrdenServicio
  public tecnico: Tecnico

  constructor() {
    this.id_orden_servicio_tecnico=0;
    this.ordenServicio=new OrdenServicio();
    this.tecnico=new Tecnico();
  }
}
