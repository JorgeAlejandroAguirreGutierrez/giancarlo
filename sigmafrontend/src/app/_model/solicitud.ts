import { Zona } from './zona';
import { Cliente } from './cliente';
import { TipoSolicitud } from './tipo-solicitud';
import { SubTipoSolicitud } from './sub-tipo-solicitud';
import { TipoPrioridad } from './tipo-prioridad';
import { Estado } from './estado';
import { Solicitante } from './solicitante';
import { ParametroSistema } from './parametro-sistema';

export class Solicitud {
    public id_solicitud: number;
    public numero_solicitud: string;
    public descripcion: string;
    public desaprobar: string;
    public prioridad: number;
    public activo: boolean;
    public cliente: Cliente;
    public solicitante: Solicitante;
    public tipo_solicitud: TipoSolicitud;
    public sub_tipo_solicitud: SubTipoSolicitud;
    public tipo_prioridad: TipoPrioridad;
    public estado: Estado;
    public parametroSistema: ParametroSistema[];
    public fecha_hora_inicio: Date;
    public fecha_hora_fin: Date;
    
    /*public fechaRegistro: Date;
    public usuarioCreacion: string;
    public fechaCreacion: Date;
    public usuarioModificacion: string;
    public fechaModificacion: Date;
    public usuarioEliminacion: string;
    public fechaEliminacion: Date;*/

    constructor() {
        this.solicitante=new Solicitante();
        this.cliente=new Cliente();
        this.tipo_solicitud=new TipoSolicitud();
        this.sub_tipo_solicitud=new SubTipoSolicitud();
        this.tipo_prioridad=new TipoPrioridad();
        this.estado=new Estado();
        this.activo=true;
    }
}
