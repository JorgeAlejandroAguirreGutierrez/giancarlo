import { Solicitud } from './solicitud';
import { Recurso } from './recurso';

export class SolicitudRecurso {
    public id: number;
    public solicitud: Solicitud;
    public recurso: Recurso;

    constructor(){
      this.recurso=new Recurso();
      this.solicitud=new Solicitud();
    }

}
