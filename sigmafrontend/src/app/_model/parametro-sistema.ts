export class ParametroSistema {
    id_parametro_sistema: number;
    id_tipo_solicitud: number;
    id_tipo_prioridad: number;
    id_sub_tipo_solicitud: number;
    id_complejidad_servicio: number;
    tabla: string;
    subtabla:string;
    descripcion: string;
    valores: string;
    cantidad: string;
    especialidad: string;
    recurso: string;
    id_padre: string;
    activo: string;

}