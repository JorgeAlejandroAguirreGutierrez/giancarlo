import { Programacion } from './programacion';

export class HojaRuta {
    public idHojaRuta: number;
    public activo: boolean;
    public descripcion: string;
    public fechaCreacion: Date;
    public fechaEliminacion: Date;
    public fechaModificacion: Date;
    public fechaVisita: Date;
    public orden: number;
    public referenciaRuta: string;
    public ubicacion: string;
    public longitud: string;
    public latitud: string;
    public usuarioCreacion: number;
    public usuarioEliminacion: number;
    public usuarioModificacion: number;
    public programacion: Programacion;
}