export class Tecnico {
  public id_tecnico: number;
  public codigo_tecnico: string;
  public nombre: string;
  public apellido_paterno: string;
  public apellido_materno: string;
  public profesion: string;
  public especialidad: string;
  public es_supervisor: boolean;
  public reten_contingencia: boolean;
  public observacion_reten: string;
  public activo: boolean;
  public fecha_ingreso: string;
  public experiencia_anios_anteriores: string;
  public usuario_creacion: string;
  public fecha_creacion: string;
  public usuario_modificacion: string;
  public fecha_modificacion: string;
  public usuario_eliminacion: string;
  public fecha_eliminacion: Date;

}
