import { Solicitud } from './solicitud';
import { OrdenServicioTecnico } from './orden-servicio-tecnico';
import { OrdenServicioRecurso } from './orden-servicio-recurso';
import { ComplejidadServicio } from './complejidad-servicio';
import { Estado } from './estado';
export class OrdenServicio {
  public id_orden_servicio: number;
  public tareas: string;
  public activo: boolean;
  public cantidad_tecnicos: number;
  public cantidad_recursos: number;
  public estado: Estado;
  public solicitud: Solicitud;
  public complejidadServicio: ComplejidadServicio;
  
  public ordenServicioTecnicos: OrdenServicioTecnico[];
  public ordenServicioRecursos: OrdenServicioRecurso[];
  public usuario_creacion: string;
  public fecha_creacion: string;
  public usuario_modificacion: string;
  public fecha_modificacion: string;
  public usuario_eliminacion: string;
  public fecha_eliminacion: string;

  constructor() {
    this.solicitud=new Solicitud();
    this.estado=new Estado();
    this.complejidadServicio=new ComplejidadServicio();
    this.ordenServicioTecnicos=[];
    this.ordenServicioRecursos=[];
  }
}