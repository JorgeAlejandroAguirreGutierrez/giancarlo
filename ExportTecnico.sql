--
-- PostgreSQL database dump
--

-- Dumped from database version 10.10
-- Dumped by pg_dump version 10.10

-- Started on 2019-10-20 13:29:43

SET statement_timeout = 0;
SET lock_timeout = 0;
SET idle_in_transaction_session_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SELECT pg_catalog.set_config('search_path', '', false);
SET check_function_bodies = false;
SET xmloption = content;
SET client_min_messages = warning;
SET row_security = off;

--
-- TOC entry 2956 (class 0 OID 17833)
-- Dependencies: 237
-- Data for Name: tecnico; Type: TABLE DATA; Schema: sistema; Owner: postgres
--

INSERT INTO sistema.tecnico (id_tecnico, activo, apellido_materno, apellido_paterno, es_supervisor, especialidad, experiencia_anios_anteriores, fecha_creacion, fecha_eliminacion, fecha_ingreso, fecha_modificacion, nombre, observacion_reten, profesion, reten_contingencia, usuario_creacion, usuario_eliminacion, usuario_modificacion, id_zona, archivo_foto, codigo_tecnico) VALUES (17, true, 'Solar', 'Solis', false, 'Circuito Cerrado de TV', 4, '2019-07-16 10:40:43', NULL, '2017-06-01 00:00:00', NULL, 'Alejandra', NULL, 'Electronico', false, 1, NULL, NULL, 1, '00000017.png', 'TEC-000017');
INSERT INTO sistema.tecnico (id_tecnico, activo, apellido_materno, apellido_paterno, es_supervisor, especialidad, experiencia_anios_anteriores, fecha_creacion, fecha_eliminacion, fecha_ingreso, fecha_modificacion, nombre, observacion_reten, profesion, reten_contingencia, usuario_creacion, usuario_eliminacion, usuario_modificacion, id_zona, archivo_foto, codigo_tecnico) VALUES (1, true, 'Diaz', 'Junco', false, 'Cámaras de Videovigilancia', 1, '2019-07-13 19:42:34', NULL, '2012-07-01 00:00:00', NULL, 'Jean', NULL, 'Electricista', true, 1, NULL, NULL, 2, '00000001.png', 'TEC-00001');
INSERT INTO sistema.tecnico (id_tecnico, activo, apellido_materno, apellido_paterno, es_supervisor, especialidad, experiencia_anios_anteriores, fecha_creacion, fecha_eliminacion, fecha_ingreso, fecha_modificacion, nombre, observacion_reten, profesion, reten_contingencia, usuario_creacion, usuario_eliminacion, usuario_modificacion, id_zona, archivo_foto, codigo_tecnico) VALUES (3, true, 'Flores', 'Cortez', false, 'Control de Accesos', 3, '2019-07-13 19:42:42', NULL, '2017-03-01 00:00:00', NULL, 'Mario', NULL, 'Electronico', false, 1, NULL, NULL, 2, '00000003.png', 'TEC-00003');
INSERT INTO sistema.tecnico (id_tecnico, activo, apellido_materno, apellido_paterno, es_supervisor, especialidad, experiencia_anios_anteriores, fecha_creacion, fecha_eliminacion, fecha_ingreso, fecha_modificacion, nombre, observacion_reten, profesion, reten_contingencia, usuario_creacion, usuario_eliminacion, usuario_modificacion, id_zona, archivo_foto, codigo_tecnico) VALUES (4, true, 'Casas', 'Rosas
', true, 'Circuito Cerrado de TV', 2, '2019-07-13 19:42:43', NULL, '2015-02-01 00:00:00', NULL, 'Juana', NULL, 'Electronico', false, 1, NULL, NULL, 1, '00000004.png', 'TEC-00004');
INSERT INTO sistema.tecnico (id_tecnico, activo, apellido_materno, apellido_paterno, es_supervisor, especialidad, experiencia_anios_anteriores, fecha_creacion, fecha_eliminacion, fecha_ingreso, fecha_modificacion, nombre, observacion_reten, profesion, reten_contingencia, usuario_creacion, usuario_eliminacion, usuario_modificacion, id_zona, archivo_foto, codigo_tecnico) VALUES (2, true, 'Echevarria', 'Ore', true, 'Alarmas de Seguridad de Intrusión', 2, '2019-07-13 19:42:42', NULL, '2014-02-01 00:00:00', NULL, 'Aslan', NULL, 'Ingeniero Electronico', false, 1, NULL, NULL, 2, '00000002.png', 'TEC-00002');
INSERT INTO sistema.tecnico (id_tecnico, activo, apellido_materno, apellido_paterno, es_supervisor, especialidad, experiencia_anios_anteriores, fecha_creacion, fecha_eliminacion, fecha_ingreso, fecha_modificacion, nombre, observacion_reten, profesion, reten_contingencia, usuario_creacion, usuario_eliminacion, usuario_modificacion, id_zona, archivo_foto, codigo_tecnico) VALUES (5, true, 'Anco', 'Pinchi', false, 'Alarmas Contra Incendios', 4, '2019-07-13 19:42:44', NULL, '2018-01-01 00:00:00', NULL, 'Juaneco', NULL, 'Redes', true, 1, NULL, NULL, 1, '00000005.png', 'TEC-00005');
INSERT INTO sistema.tecnico (id_tecnico, activo, apellido_materno, apellido_paterno, es_supervisor, especialidad, experiencia_anios_anteriores, fecha_creacion, fecha_eliminacion, fecha_ingreso, fecha_modificacion, nombre, observacion_reten, profesion, reten_contingencia, usuario_creacion, usuario_eliminacion, usuario_modificacion, id_zona, archivo_foto, codigo_tecnico) VALUES (7, true, 'Lang', 'Lee', false, 'Alarmas Contra Incendios', 2, '2019-07-16 10:18:17', NULL, '2019-01-01 00:00:00', NULL, 'Kokoro', NULL, 'Industrial y Sistemas', false, 1, NULL, NULL, 1, '00000007.png', 'TEC-00007');
INSERT INTO sistema.tecnico (id_tecnico, activo, apellido_materno, apellido_paterno, es_supervisor, especialidad, experiencia_anios_anteriores, fecha_creacion, fecha_eliminacion, fecha_ingreso, fecha_modificacion, nombre, observacion_reten, profesion, reten_contingencia, usuario_creacion, usuario_eliminacion, usuario_modificacion, id_zona, archivo_foto, codigo_tecnico) VALUES (11, true, 'De Rosas', 'Romero', false, 'Alarmas Contra Incendios', 4, '2019-07-16 10:28:55', NULL, '2014-01-01 00:00:00', NULL, 'Lima', NULL, 'Electricista', false, 1, NULL, NULL, 4, '00000011.png', 'TEC-000011');
INSERT INTO sistema.tecnico (id_tecnico, activo, apellido_materno, apellido_paterno, es_supervisor, especialidad, experiencia_anios_anteriores, fecha_creacion, fecha_eliminacion, fecha_ingreso, fecha_modificacion, nombre, observacion_reten, profesion, reten_contingencia, usuario_creacion, usuario_eliminacion, usuario_modificacion, id_zona, archivo_foto, codigo_tecnico) VALUES (16, true, 'Motolla', 'Ken', false, 'Alarmas de Seguridad de Intrusión', 3, '2019-07-16 10:34:59', NULL, '2010-12-01 00:00:00', NULL, 'Mayumi', NULL, 'Electricista', false, 1, NULL, NULL, 1, '00000016.png', 'TEC-000016');
INSERT INTO sistema.tecnico (id_tecnico, activo, apellido_materno, apellido_paterno, es_supervisor, especialidad, experiencia_anios_anteriores, fecha_creacion, fecha_eliminacion, fecha_ingreso, fecha_modificacion, nombre, observacion_reten, profesion, reten_contingencia, usuario_creacion, usuario_eliminacion, usuario_modificacion, id_zona, archivo_foto, codigo_tecnico) VALUES (18, true, 'Sanchez', 'Juarez', false, 'Control de Accesos', 2, '2019-07-16 10:41:13', NULL, '2017-09-01 00:00:00', NULL, 'Pedro', NULL, 'Electronico', true, 1, NULL, NULL, 3, '00000018.png', 'TEC-000018');
INSERT INTO sistema.tecnico (id_tecnico, activo, apellido_materno, apellido_paterno, es_supervisor, especialidad, experiencia_anios_anteriores, fecha_creacion, fecha_eliminacion, fecha_ingreso, fecha_modificacion, nombre, observacion_reten, profesion, reten_contingencia, usuario_creacion, usuario_eliminacion, usuario_modificacion, id_zona, archivo_foto, codigo_tecnico) VALUES (12, true, 'Ramos', 'Mendivil', true, 'Control de Accesos', 1, '2019-07-16 10:28:54', NULL, '2016-09-01 00:00:00', NULL, 'Tiziano', NULL, 'Ingeniero de Redes', true, 1, NULL, NULL, 4, '00000012.png', 'TEC-000012');
INSERT INTO sistema.tecnico (id_tecnico, activo, apellido_materno, apellido_paterno, es_supervisor, especialidad, experiencia_anios_anteriores, fecha_creacion, fecha_eliminacion, fecha_ingreso, fecha_modificacion, nombre, observacion_reten, profesion, reten_contingencia, usuario_creacion, usuario_eliminacion, usuario_modificacion, id_zona, archivo_foto, codigo_tecnico) VALUES (8, true, 'Liu', 'Fu', false, 'Control de Accesos', 2, '2019-07-16 10:20:05', NULL, '2018-01-01 00:00:00', NULL, 'Chizune', NULL, 'Industrial y Sistemas', false, 1, NULL, NULL, 3, '00000008.png', 'TEC-00008');
INSERT INTO sistema.tecnico (id_tecnico, activo, apellido_materno, apellido_paterno, es_supervisor, especialidad, experiencia_anios_anteriores, fecha_creacion, fecha_eliminacion, fecha_ingreso, fecha_modificacion, nombre, observacion_reten, profesion, reten_contingencia, usuario_creacion, usuario_eliminacion, usuario_modificacion, id_zona, archivo_foto, codigo_tecnico) VALUES (13, true, 'Lopez', 'Bond', false, 'Circuito Cerrado de TV', 2, '2019-07-16 10:30:58', NULL, '2017-01-01 00:00:00', NULL, 'Jorge', NULL, 'Electronico', false, 1, NULL, NULL, 4, '00000013.png', 'TEC-000013');
INSERT INTO sistema.tecnico (id_tecnico, activo, apellido_materno, apellido_paterno, es_supervisor, especialidad, experiencia_anios_anteriores, fecha_creacion, fecha_eliminacion, fecha_ingreso, fecha_modificacion, nombre, observacion_reten, profesion, reten_contingencia, usuario_creacion, usuario_eliminacion, usuario_modificacion, id_zona, archivo_foto, codigo_tecnico) VALUES (9, true, 'Muro', 'Rosas', false, 'Control de Accesos', 3, '2019-07-16 10:23:02', NULL, '2015-12-01 00:00:00', NULL, 'Graciela', NULL, 'Computacion', true, 1, NULL, NULL, 3, '00000009.png', 'TEC-00009');
INSERT INTO sistema.tecnico (id_tecnico, activo, apellido_materno, apellido_paterno, es_supervisor, especialidad, experiencia_anios_anteriores, fecha_creacion, fecha_eliminacion, fecha_ingreso, fecha_modificacion, nombre, observacion_reten, profesion, reten_contingencia, usuario_creacion, usuario_eliminacion, usuario_modificacion, id_zona, archivo_foto, codigo_tecnico) VALUES (14, true, 'Oos', 'Grandez', false, 'Cámaras de Videovigilancia', 2, '2019-07-16 10:31:45', NULL, '2018-10-01 00:00:00', NULL, 'Michell', NULL, 'Electronico', false, 1, NULL, NULL, 4, '00000014.png', 'TEC-000014');
INSERT INTO sistema.tecnico (id_tecnico, activo, apellido_materno, apellido_paterno, es_supervisor, especialidad, experiencia_anios_anteriores, fecha_creacion, fecha_eliminacion, fecha_ingreso, fecha_modificacion, nombre, observacion_reten, profesion, reten_contingencia, usuario_creacion, usuario_eliminacion, usuario_modificacion, id_zona, archivo_foto, codigo_tecnico) VALUES (6, true, 'Garcia', 'Moran', false, 'Cámaras de Videovigilancia', 5, '2019-07-13 19:42:45', NULL, '2017-11-01 00:00:00', NULL, 'Pierina', NULL, 'Ingeniro Industrial', false, 1, NULL, NULL, 1, '00000006.png', 'TEC-00006');
INSERT INTO sistema.tecnico (id_tecnico, activo, apellido_materno, apellido_paterno, es_supervisor, especialidad, experiencia_anios_anteriores, fecha_creacion, fecha_eliminacion, fecha_ingreso, fecha_modificacion, nombre, observacion_reten, profesion, reten_contingencia, usuario_creacion, usuario_eliminacion, usuario_modificacion, id_zona, archivo_foto, codigo_tecnico) VALUES (10, true, 'Solar', 'Pineada', true, 'Alarmas de Seguridad de Intrusión', 4, '2019-07-16 10:26:02', NULL, '2019-02-01 00:00:00', NULL, 'Camilo', NULL, 'Ingeniera Electronica', false, 1, NULL, NULL, 3, '00000010.png', 'TEC-000010');
INSERT INTO sistema.tecnico (id_tecnico, activo, apellido_materno, apellido_paterno, es_supervisor, especialidad, experiencia_anios_anteriores, fecha_creacion, fecha_eliminacion, fecha_ingreso, fecha_modificacion, nombre, observacion_reten, profesion, reten_contingencia, usuario_creacion, usuario_eliminacion, usuario_modificacion, id_zona, archivo_foto, codigo_tecnico) VALUES (15, true, 'Condori', 'Gomez', true, 'Alarmas Contra Incendios', 1, '2019-07-16 10:33:40', NULL, '2011-11-01 00:00:00', NULL, 'Marisol', NULL, 'Redes y Comunicaciones', true, 1, NULL, NULL, 1, '00000015.png', 'TEC-000015');


--
-- TOC entry 2963 (class 0 OID 0)
-- Dependencies: 238
-- Name: tecnico_id_tecnico_seq; Type: SEQUENCE SET; Schema: sistema; Owner: postgres
--

SELECT pg_catalog.setval('sistema.tecnico_id_tecnico_seq', 1, false);


-- Completed on 2019-10-20 13:29:44

--
-- PostgreSQL database dump complete
--

