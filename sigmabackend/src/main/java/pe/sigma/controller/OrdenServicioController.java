package pe.sigma.controller;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.hateoas.Resource;
import org.springframework.hateoas.mvc.ControllerLinkBuilder;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import pe.sigma.exception.ModeloNotFoundException;
import pe.sigma.model.OrdenServicio;
import pe.sigma.service.IOrdenServicioService;

@RestController
@RequestMapping("/ordenServicios")
public class OrdenServicioController {
	@Autowired
	private IOrdenServicioService service;

	@GetMapping(produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<List<OrdenServicio>> listar() {
		List<OrdenServicio> ordenServicioes = new ArrayList<>();
		ordenServicioes = service.listar();

		return new ResponseEntity<List<OrdenServicio>>(ordenServicioes, HttpStatus.OK);
	}

	@GetMapping(value = "/{id}")
	public ResponseEntity<OrdenServicio> listarId(@PathVariable("id") Integer id) {
		OrdenServicio ordenServicio = new OrdenServicio();
		Optional<OrdenServicio> ordenServicioOptional = service.listarId(id);
		if (ordenServicioOptional.isPresent()) {
			ordenServicio = ordenServicioOptional.get();
			return new ResponseEntity<OrdenServicio>(ordenServicio, HttpStatus.OK);
		}
		return new ResponseEntity<>(null, HttpStatus.OK);
	}

	@PostMapping(consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<OrdenServicio> registrar(@Valid @RequestBody OrdenServicio ordenServicio) {
		OrdenServicio rec;
		rec = service.registrar(ordenServicio);
		return new ResponseEntity<OrdenServicio>(rec, HttpStatus.CREATED);
	}

	@PutMapping(consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<Object> actualizar(@Valid @RequestBody OrdenServicio recurso) {
		service.modificar(recurso);
		return new ResponseEntity<Object>(HttpStatus.OK);
	}

	@DeleteMapping(value = "/{id}", produces = MediaType.APPLICATION_JSON_VALUE)
	public void eliminar(@PathVariable Integer id) {
		Optional<OrdenServicio> recOptional = service.listarId(id);
		if (recOptional.isPresent()) {
			service.eliminar(id);
		} else {
			throw new ModeloNotFoundException("ID: " + id);
		}
	}
	
	@GetMapping(value = "/listarBusqueda", produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<Page<OrdenServicio>> listarBusqueda(
			@RequestParam(value = "numero_orden", required = false) String numero_orden,
			@RequestParam(value = "fecha_recepcion", required = false) String fecha_recepcion,
			@RequestParam(value = "id_tipo_solicitud", required = false) Integer id_tipo_solicitud,
			@RequestParam(value = "id_estado", required = false) Integer id_estado,
			@RequestParam(value = "skip", required = false) Integer skip,
			@RequestParam(value = "take", required = false) Integer take) {
		Page<OrdenServicio> ordenesServicio;
		ordenesServicio  = service.listar(numero_orden, fecha_recepcion, id_tipo_solicitud, id_estado,skip, take);
		return new ResponseEntity<>(ordenesServicio, HttpStatus.OK);
	}

}
