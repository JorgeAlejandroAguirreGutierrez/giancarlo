package pe.sigma.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.hateoas.Resource;
import org.springframework.hateoas.mvc.ControllerLinkBuilder;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;
import pe.sigma.exception.ModeloNotFoundException;
import pe.sigma.model.SubTipoSolicitud;
import pe.sigma.service.ISubTipoSolicitudService;

import javax.validation.Valid;
import java.net.URI;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import static org.springframework.hateoas.mvc.ControllerLinkBuilder.linkTo;
import static org.springframework.hateoas.mvc.ControllerLinkBuilder.methodOn;

@RestController
@RequestMapping("/subTipoSolicitud")
public class SubTipoController {

	@Autowired
	private ISubTipoSolicitudService service;

	@GetMapping(produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<List<SubTipoSolicitud>> listar() {
		List<SubTipoSolicitud> subTiposSolicitud= new ArrayList<>();
		subTiposSolicitud = service.listar();

		return new ResponseEntity<List<SubTipoSolicitud>>(subTiposSolicitud, HttpStatus.OK);
	}

	@GetMapping(value = "/{id}")
	public Resource<SubTipoSolicitud> listarId(@PathVariable("id") Integer id) {
		SubTipoSolicitud rec = new SubTipoSolicitud();
		Optional<SubTipoSolicitud> recOptional = service.listarId(id);
		if (recOptional.isPresent()) {
			rec = recOptional.get();
		}

		Resource<SubTipoSolicitud> resource = new Resource<SubTipoSolicitud>(rec);
		ControllerLinkBuilder linkTo = linkTo(methodOn(this.getClass()).listarId(id));
		resource.add(linkTo.withRel("subTipoSolicitud-resource"));

		return resource;
	}

	@PostMapping(consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<SubTipoSolicitud> registrar(@Valid @RequestBody SubTipoSolicitud subTipoSolicitud) {
		SubTipoSolicitud rec = new SubTipoSolicitud();
		rec = service.registrar(subTipoSolicitud);
		URI location = ServletUriComponentsBuilder.fromCurrentRequest().path("/{id}").buildAndExpand(rec.getId_sub_tipo_solicitud())
				.toUri();

		return ResponseEntity.created(location).build();
	}

	@PutMapping(consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<Object> actualizar(@Valid @RequestBody SubTipoSolicitud subTipoSolicitud) {
		service.modificar(subTipoSolicitud);
		return new ResponseEntity<Object>(HttpStatus.OK);
	}

	@DeleteMapping(value = "/{id}", produces = MediaType.APPLICATION_JSON_VALUE)
	public void eliminar(@PathVariable Integer id) {
		Optional<SubTipoSolicitud> recOptional = service.listarId(id);
		if (recOptional.isPresent()) {
			service.eliminar(id);
		} else {
			throw new ModeloNotFoundException("ID: " + id);
		}
	}
}
