package pe.sigma.controller;

import java.util.*;

import javax.validation.Valid;

import static org.springframework.hateoas.mvc.ControllerLinkBuilder.linkTo;
import static org.springframework.hateoas.mvc.ControllerLinkBuilder.methodOn;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.hateoas.Resource;
import org.springframework.hateoas.mvc.ControllerLinkBuilder;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.FieldError;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.*;

import pe.sigma.exception.ModeloNotFoundException;
import pe.sigma.model.Solicitud;
import pe.sigma.service.ISolicitudService;

@RestController
@RequestMapping("/solicitudes")
public class SolicitudController {

	@Autowired
	private ISolicitudService service;

	@GetMapping(produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<List<Solicitud>> listar() {
		List<Solicitud> solicitudes = new ArrayList<>();
		solicitudes = service.listar();

		return new ResponseEntity<List<Solicitud>>(solicitudes, HttpStatus.OK);
	}

	@GetMapping(value = "/{id}")
	public Resource<Solicitud> listarId(@PathVariable("id") Integer id) {
		Solicitud rec = new Solicitud();
		Optional<Solicitud> recOptional = service.listarId(id);
		if (recOptional.isPresent()) {
			rec = recOptional.get();
		}

		Resource<Solicitud> resource = new Resource<Solicitud>(rec);
		ControllerLinkBuilder linkTo = linkTo(methodOn(this.getClass()).listarId(id));
		resource.add(linkTo.withRel("solicitud-resource"));

		return resource;
	}

	@PostMapping(consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<Solicitud> registrar(@Valid @RequestBody Solicitud solicitud) {
		Solicitud rec;
		rec = service.registrar(solicitud);
		return new ResponseEntity<Solicitud>(rec, HttpStatus.CREATED);
	}

	@PutMapping(consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<Object> actualizar(@Valid @RequestBody Solicitud solicitud) {
		service.modificar(solicitud);
		return new ResponseEntity<Object>(HttpStatus.OK);
	}

	@DeleteMapping(value = "/{id}", produces = MediaType.APPLICATION_JSON_VALUE)
	public void eliminar(@PathVariable Integer id) {
		Optional<Solicitud> recOptional = service.listarId(id);
		if (recOptional.isPresent()) {
			service.eliminar(id);
		} else {
			throw new ModeloNotFoundException("ID: " + id);
		}
	}

	@GetMapping(value = "/listarBusqueda", produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<Page<Solicitud>> listarBusqueda(
			@RequestParam(value = "numero_solicitud", required = false) String numero_solicitud,
			@RequestParam(value = "fecha_recepcion", required = false) String fecha_recepcion,
			@RequestParam(value = "id_tipo_solicitud", required = false) Integer id_tipo_solicitud,
			@RequestParam(value = "id_estado", required = false) Integer id_estado,
			@RequestParam(value = "skip", required = false) Integer skip,
			@RequestParam(value = "take", required = false) Integer take) {
		Page<Solicitud> solicitudesServicio;
		solicitudesServicio  = service.listarSolicitudServicioPagNum(numero_solicitud, fecha_recepcion, id_tipo_solicitud, id_estado,skip, take);
		return new ResponseEntity<>(solicitudesServicio, HttpStatus.OK);
	}

	@ResponseStatus(HttpStatus.BAD_REQUEST)
	@ExceptionHandler(MethodArgumentNotValidException.class)
	public Map<String, String> handleValidationExceptions(
			MethodArgumentNotValidException ex) {
		Map<String, String> errors = new HashMap<>();
		ex.getBindingResult().getAllErrors().forEach((error) -> {
			String fieldName = ((FieldError) error).getField();
			String errorMessage = error.getDefaultMessage();
			errors.put(fieldName, errorMessage);
		});
		return errors;
	}
}
