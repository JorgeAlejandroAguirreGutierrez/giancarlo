package pe.sigma.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.hateoas.Resource;
import org.springframework.hateoas.mvc.ControllerLinkBuilder;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;
import pe.sigma.exception.ModeloNotFoundException;
import pe.sigma.model.ComplejidadServicio;
import pe.sigma.model.TipoPrioridad;
import pe.sigma.service.IComplejidadServicioService;
import pe.sigma.service.ITipoPrioridadService;

import javax.validation.Valid;
import java.net.URI;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import static org.springframework.hateoas.mvc.ControllerLinkBuilder.linkTo;
import static org.springframework.hateoas.mvc.ControllerLinkBuilder.methodOn;

@RestController
@RequestMapping("/complejidadesServicio")
public class ComplejidadServicioController {
	@Autowired
	private IComplejidadServicioService service;

	@GetMapping(produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<List<ComplejidadServicio>> listar() {
		List<ComplejidadServicio> complejidadesServicio = new ArrayList<>();
		complejidadesServicio = service.listar();

		return new ResponseEntity<List<ComplejidadServicio>>(complejidadesServicio, HttpStatus.OK);
	}

	@GetMapping(value = "/{id}")
	public Resource<ComplejidadServicio> listarId(@PathVariable("id") Integer id) {
		ComplejidadServicio rec = new ComplejidadServicio();
		Optional<ComplejidadServicio> recOptional = service.listarId(id);
		if (recOptional.isPresent()) {
			rec = recOptional.get();
		}

		Resource<ComplejidadServicio> resource = new Resource<ComplejidadServicio>(rec);
		ControllerLinkBuilder linkTo = linkTo(methodOn(this.getClass()).listarId(id));
		resource.add(linkTo.withRel("tipoPrioridad-resource"));

		return resource;
	}

	@PostMapping(consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<ComplejidadServicio> registrar(@Valid @RequestBody ComplejidadServicio complejidadServicio) {
		ComplejidadServicio rec = new ComplejidadServicio();
		rec = service.registrar(complejidadServicio);
		URI location = ServletUriComponentsBuilder.fromCurrentRequest().path("/{id}").buildAndExpand(rec.getId_complejidad_servicio())
				.toUri();

		return ResponseEntity.created(location).build();
	}

	@PutMapping(consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<Object> actualizar(@Valid @RequestBody ComplejidadServicio complejidadServicio) {
		service.modificar(complejidadServicio);
		return new ResponseEntity<Object>(HttpStatus.OK);
	}

	@DeleteMapping(value = "/{id}", produces = MediaType.APPLICATION_JSON_VALUE)
	public void eliminar(@PathVariable Integer id) {
		Optional<ComplejidadServicio> recOptional = service.listarId(id);
		if (recOptional.isPresent()) {
			service.eliminar(id);
		} else {
			throw new ModeloNotFoundException("ID: " + id);
		}
	}

}
