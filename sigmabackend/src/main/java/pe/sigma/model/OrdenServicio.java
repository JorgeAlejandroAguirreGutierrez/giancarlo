package pe.sigma.model;

import java.time.LocalDateTime;
import java.util.List;

import javax.persistence.*;

import com.fasterxml.jackson.annotation.JsonManagedReference;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.fasterxml.jackson.databind.ser.std.ToStringSerializer;

@Entity
@Table(name = "orden_servicio")
public class OrdenServicio {
	
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Integer id_orden_servicio;
	
	@Column(name = "numero_orden", nullable = true)
	private String numero_orden;

	@Column(name = "tareas", nullable = true)
	private String tareas;

	@ManyToOne
	@JoinColumn(name = "id_estado", nullable = true)
	private Estado estado;
	
	@OneToOne
	@JoinColumn(name = "id_solicitud", nullable = true)
	private Solicitud solicitud;

	@ManyToOne
	@JoinColumn(name = "id_complejidad_servicio", nullable = true)
	private ComplejidadServicio complejidadServicio;
	
	@Column(name = "activo", nullable = true)
	 private boolean activo;

	@OneToMany(cascade=CascadeType.ALL)
	private List<OrdenServicioRecurso> ordenServicioRecursos;

	@OneToMany(cascade=CascadeType.ALL)
	private List<OrdenServicioTecnico> ordenServicioTecnicos;

	
	@Column(name = "usuario_creacion", nullable = true)
	 private Integer usuario_creacion;
	
	@JsonSerialize(using = ToStringSerializer.class)
	 private LocalDateTime fecha_creacion;
	
	@Column(name = "usuario_modificacion", nullable = true)
	 private Integer usuario_modificacion;
	
	@JsonSerialize(using = ToStringSerializer.class)
	 private LocalDateTime fecha_modificacion;
	
	@Column(name = "usuario_eliminacion", nullable = true)
	 private Integer usuario_eliminacion;
	
	@JsonSerialize(using = ToStringSerializer.class)
	 private LocalDateTime fecha_eliminacion;

	public Integer getId_orden_servicio() {
		return id_orden_servicio;
	}

	public void setId_orden_servicio(Integer id_orden_servicio) {
		this.id_orden_servicio = id_orden_servicio;
	}

	public Estado getEstado() {
		return estado;
	}

	public void setEstado(Estado estado) {
		this.estado = estado;
	}

	public Solicitud getSolicitud() {
		return solicitud;
	}

	public void setSolicitud(Solicitud solicitud) {
		this.solicitud = solicitud;
	}



	public ComplejidadServicio getComplejidadServicio() {
		return complejidadServicio;
	}
	public void setComplejidadServicio(ComplejidadServicio complejidadServicio) {
		this.complejidadServicio = complejidadServicio;
	}

	public String getTareas() {
		return tareas;
	}

	public void setTareas(String tareas) {
		this.tareas = tareas;
	}

	public boolean isActivo() {
		return activo;
	}

	public void setActivo(boolean activo) {
		this.activo = activo;
	}

	public Integer getUsuario_creacion() {
		return usuario_creacion;
	}

	public void setUsuario_creacion(Integer usuario_creacion) {
		this.usuario_creacion = usuario_creacion;
	}

	public LocalDateTime getFecha_creacion() {
		return fecha_creacion;
	}

	public void setFecha_creacion(LocalDateTime fecha_creacion) {
		this.fecha_creacion = fecha_creacion;
	}

	public Integer getUsuario_modificacion() {
		return usuario_modificacion;
	}

	public void setUsuario_modificacion(Integer usuario_modificacion) {
		this.usuario_modificacion = usuario_modificacion;
	}

	public LocalDateTime getFecha_modificacion() {
		return fecha_modificacion;
	}

	public void setFecha_modificacion(LocalDateTime fecha_modificacion) {
		this.fecha_modificacion = fecha_modificacion;
	}

	public Integer getUsuario_eliminacion() {
		return usuario_eliminacion;
	}

	public void setUsuario_eliminacion(Integer usuario_eliminacion) {
		this.usuario_eliminacion = usuario_eliminacion;
	}

	public LocalDateTime getFecha_eliminacion() {
		return fecha_eliminacion;
	}

	public void setFecha_eliminacion(LocalDateTime fecha_eliminacion) {
		this.fecha_eliminacion = fecha_eliminacion;
	}
	
	public String getNumero_orden() {
		return numero_orden;
	}
	
	public void setNumero_orden(String numero_orden) {
		this.numero_orden = numero_orden;
	}

	@JsonManagedReference
	public List<OrdenServicioRecurso> getOrdenServicioRecursos() {
		return ordenServicioRecursos;
	}

	@JsonManagedReference
	public List<OrdenServicioTecnico> getOrdenServicioTecnicos() {
		return ordenServicioTecnicos;
	}

	public void setOrdenServicioRecursos(List<OrdenServicioRecurso> ordenServicioRecursos) {
		this.ordenServicioRecursos = ordenServicioRecursos;
	}

	public void setOrdenServicioTecnicos(List<OrdenServicioTecnico> ordenServicioTecnicos) {
		this.ordenServicioTecnicos = ordenServicioTecnicos;
	}
}
