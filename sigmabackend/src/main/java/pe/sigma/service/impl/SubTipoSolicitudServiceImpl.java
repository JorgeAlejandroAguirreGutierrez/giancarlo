package pe.sigma.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import pe.sigma.dao.ISubTipoSolicitudDao;
import pe.sigma.model.SubTipoSolicitud;
import pe.sigma.service.ISubTipoSolicitudService;

import java.util.List;
import java.util.Optional;

@Service
public class SubTipoSolicitudServiceImpl implements ISubTipoSolicitudService {

	@Autowired
	private ISubTipoSolicitudDao dao;
	
	@Override
	public SubTipoSolicitud registrar(SubTipoSolicitud t) {
		return dao.save(t);
	}

	@Override
	public SubTipoSolicitud modificar(SubTipoSolicitud t) {
		return dao.save(t);
	}

	@Override
	public void eliminar(int id) {
		dao.deleteById(id);
	}

	@Override
	public Optional<SubTipoSolicitud> listarId(int id) {
		return dao.findById(id);
	}

	@Override
	public List<SubTipoSolicitud> listar() {
		return dao.findAll();
	}

}
