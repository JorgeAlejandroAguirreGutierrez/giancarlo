package pe.sigma.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import pe.sigma.dao.IComplejidadServicioDao;
import pe.sigma.dao.ITipoPrioridadDao;
import pe.sigma.model.ComplejidadServicio;
import pe.sigma.model.TipoPrioridad;
import pe.sigma.service.IComplejidadServicioService;
import pe.sigma.service.ITipoPrioridadService;

import java.util.List;
import java.util.Optional;


@Service
public class ComplejidadServicioServiceImpl implements IComplejidadServicioService {
	@Autowired
	private IComplejidadServicioDao dao;
	
	@Override
	public ComplejidadServicio registrar(ComplejidadServicio t) {
		return dao.save(t);
	}

	@Override
	public ComplejidadServicio modificar(ComplejidadServicio t) {
		return dao.save(t);
	}

	@Override
	public void eliminar(int id) {
		dao.deleteById(id);
	}

	@Override
	public Optional<ComplejidadServicio> listarId(int id) {
		return dao.findById(id);
	}

	@Override
	public List<ComplejidadServicio> listar() {
		return dao.findAll();
	}
}
