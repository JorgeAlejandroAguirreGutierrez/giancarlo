package pe.sigma.service.impl;


import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.stereotype.Service;

import pe.sigma.dao.IOrdenServicioDao;
import pe.sigma.model.OrdenServicio;
import pe.sigma.model.Solicitud;
import pe.sigma.service.IOrdenServicioService;

@Service
public class OrdenServicioServiceImpl implements IOrdenServicioService {

	@Autowired
	private IOrdenServicioDao dao;
	
	@Override
	public OrdenServicio registrar(OrdenServicio t) {
		long cantidad=dao.count()+1;
		String numero_orden=padLeftZeros(String.valueOf(cantidad), 6);
		t.setNumero_orden("ORD-"+numero_orden);
		return dao.save(t);
	}
	
	private String padLeftZeros(String inputString, int length) {
	    if (inputString.length() >= length) {
	        return inputString;
	    }
	    StringBuilder sb = new StringBuilder();
	    while (sb.length() < length - inputString.length()) {
	        sb.append('0');
	    }
	    sb.append(inputString);
	 
	    return sb.toString();
	}

	@Override
	public OrdenServicio modificar(OrdenServicio t) {
		return dao.save(t);
	}

	@Override
	public void eliminar(int id) {
		dao.deleteById(id);
	}

	@Override
	public Optional<OrdenServicio> listarId(int id) {
		return dao.findById(id);
	}

	@Override
	public List<OrdenServicio> listar() {
		return dao.findAll();
	}
	
	@Override
	public List<String> listarOrdenServicio(Integer id_orden_servicio,String fecha_recepcion,Integer id_tipo_solicitud,Integer id_estado) {
		return dao.listarOrdenServicio(id_orden_servicio, fecha_recepcion, id_tipo_solicitud, id_estado);
	}
	
	@Override
	public List<String> listarOrdenServicioPag(Integer id_orden_servicio,String fecha_recepcion,Integer id_tipo_solicitud,Integer id_estado,Integer skip,Integer take) {
		return dao.listarOrdenServicioPag(id_orden_servicio, fecha_recepcion, id_tipo_solicitud, id_estado,skip,take);
	}

	@Override
	public List<String> listarOrdenServicioPagNum(String numero_orden, String fecha_recepcion,Integer id_tipo_solicitud, Integer id_estado, Integer skip, Integer take) {
		return dao.listarOrdenServicioPagNum(numero_orden, fecha_recepcion, id_tipo_solicitud, id_estado,skip,take);
	}
	
	@Override
	public Page<OrdenServicio> listar(String numero_orden, String fecha_recepcion, Integer id_tipo_solicitud, Integer id_estado, Integer skip, Integer take) {
		return  dao.findAll(new Specification<OrdenServicio>() {
			@Override
			public Predicate toPredicate(Root<OrdenServicio> root, CriteriaQuery<?> criteriaQuery, CriteriaBuilder criteriaBuilder) {
				List<Predicate> predicates = new ArrayList<>();
				if (numero_orden!=null) {
					predicates.add(criteriaBuilder.and(criteriaBuilder.equal(root.get("numero_orden"), numero_orden)));
				}
				if (fecha_recepcion!=null) {
					predicates.add(criteriaBuilder.and(criteriaBuilder.equal(root.get("solicitud").get("fecha_recepcion"), fecha_recepcion)));
				}
				if (id_tipo_solicitud!=null) {
					predicates.add(criteriaBuilder.and(criteriaBuilder.equal(root.get("solicitud").get("tipo_solicitud").get("id_tipo_solicitud"), id_tipo_solicitud)));
				}
				if (id_estado!=null) {
					predicates.add(criteriaBuilder.and(criteriaBuilder.equal(root.get("estado").get("id_estado"), id_estado)));
				}
				return criteriaBuilder.and(predicates.toArray(new Predicate[predicates.size()]));
			}
		}, PageRequest.of(skip, take));
	}

}
