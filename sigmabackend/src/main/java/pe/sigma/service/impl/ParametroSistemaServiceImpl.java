package pe.sigma.service.impl;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import org.hibernate.Criteria;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.stereotype.Service;

import pe.sigma.dao.IParametroSistemaDao;
import pe.sigma.model.ParametroSistema;
import pe.sigma.model.Solicitud;
import pe.sigma.service.IParametroSistemaService;

import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;

@Service
public class ParametroSistemaServiceImpl implements IParametroSistemaService{
	@Autowired
	private IParametroSistemaDao dao;

	@Override
	public ParametroSistema registrar(ParametroSistema t) {
		// TODO Auto-generated method stub
		return dao.save(t);
	}

	@Override
	public ParametroSistema modificar(ParametroSistema t) {
		// TODO Auto-generated method stub
		return dao.save(t);
	}

	@Override
	public void eliminar(int id) {
		// TODO Auto-generated method stub
		dao.deleteById(id);
		
	}

	@Override
	public Optional<ParametroSistema> listarId(int id) {
		// TODO Auto-generated method stub
		return dao.findById(id);
	}

	@Override
	public List<ParametroSistema> listar() {
		// TODO Auto-generated method stub
		return dao.findAll();
	}
	
	@Override
	public List<String> listarParametroSistemaTec(Integer id_solicitud) {
		return dao.listarParametroSistemaTec(id_solicitud);
	}

	@Override
	public List<ParametroSistema> listarParametroSistemaTipo(Integer id_tipo_solicitud, Integer id_sub_tipo_solicitud) {
		return  dao.findAll(new Specification<ParametroSistema>() {
			@Override
			public Predicate toPredicate(Root<ParametroSistema> root, CriteriaQuery<?> criteriaQuery, CriteriaBuilder criteriaBuilder) {
				List<Predicate> predicates = new ArrayList<>();
				if (id_tipo_solicitud!=null) {
					predicates.add(criteriaBuilder.and(criteriaBuilder.equal(root.get("id_tipo_solicitud"), id_tipo_solicitud)));
				}
				if (id_sub_tipo_solicitud!=null) {
					predicates.add(criteriaBuilder.and(criteriaBuilder.equal(root.get("id_sub_tipo_solicitud"), id_sub_tipo_solicitud)));
				}
				return criteriaBuilder.and(predicates.toArray(new Predicate[predicates.size()]));
			}
		});
	}
}
