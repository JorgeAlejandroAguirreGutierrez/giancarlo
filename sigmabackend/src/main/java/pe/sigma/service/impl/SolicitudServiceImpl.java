package pe.sigma.service.impl;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.stereotype.Service;

import pe.sigma.dao.ISolicitudDao;
import pe.sigma.model.Solicitud;
import pe.sigma.service.ISolicitudService;

import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;

@Service
public class SolicitudServiceImpl implements ISolicitudService {

	@Autowired
	private ISolicitudDao dao;
	
	@Override
	public Solicitud registrar(Solicitud t) {
		long cantidad=dao.count()+1;
		String numero_solicitud=padLeftZeros(String.valueOf(cantidad), 6);
		t.setNumero_solicitud("SOL-"+numero_solicitud);
		return dao.save(t);
	}
	
	private String padLeftZeros(String inputString, int length) {
	    if (inputString.length() >= length) {
	        return inputString;
	    }
	    StringBuilder sb = new StringBuilder();
	    while (sb.length() < length - inputString.length()) {
	        sb.append('0');
	    }
	    sb.append(inputString);
	 
	    return sb.toString();
	}

	@Override
	public Solicitud modificar(Solicitud t) {
		return dao.save(t);
	}

	@Override
	public void eliminar(int id) {
		dao.deleteById(id);
	}

	@Override
	public Optional<Solicitud> listarId(int id) {
		return dao.findById(id);
	}

	@Override
	public List<Solicitud> listar() {
		return dao.findAll();
	}

	@Override
	public Page<Solicitud> listarSolicitudServicioPagNum(String numero_solicitud, String fecha_recepcion, Integer id_tipo_solicitud, Integer id_estado, Integer skip, Integer take) {
		return  dao.findAll(new Specification<Solicitud>() {
			@Override
			public Predicate toPredicate(Root<Solicitud> root, CriteriaQuery<?> criteriaQuery, CriteriaBuilder criteriaBuilder) {
				List<Predicate> predicates = new ArrayList<>();
				if (numero_solicitud!=null) {
					predicates.add(criteriaBuilder.and(criteriaBuilder.equal(root.get("numero_solicitud"), numero_solicitud)));
				}
				if (fecha_recepcion!=null) {
					predicates.add(criteriaBuilder.and(criteriaBuilder.equal(root.get("fecha_recepcion"), fecha_recepcion)));
				}
				if (id_tipo_solicitud!=null) {
					predicates.add(criteriaBuilder.and(criteriaBuilder.equal(root.get("tipo_solicitud").get("id_tipo_solicitud"), id_tipo_solicitud)));
				}
				if (id_estado!=null) {
					predicates.add(criteriaBuilder.and(criteriaBuilder.equal(root.get("estado").get("id_estado"), id_estado)));
				}
				return criteriaBuilder.and(predicates.toArray(new Predicate[predicates.size()]));
			}
		}, PageRequest.of(skip, take));
	}
}
