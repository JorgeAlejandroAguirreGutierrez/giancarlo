package pe.sigma.service;

import org.springframework.data.domain.Page;
import pe.sigma.model.Solicitud;

public interface ISolicitudService extends ICRUD<Solicitud> {

    Page<Solicitud> listarSolicitudServicioPagNum(String numero_solicitud, String fecha_recepcion, Integer id_tipo_solicitud, Integer id_estado, Integer skip, Integer take);
}
