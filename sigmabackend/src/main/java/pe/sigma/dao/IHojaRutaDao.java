package pe.sigma.dao;

import org.springframework.data.jpa.repository.JpaRepository;

import pe.sigma.model.HojaRuta;

public interface IHojaRutaDao extends JpaRepository<HojaRuta, Integer>{

}
