package pe.sigma.dao;

import org.springframework.data.jpa.repository.JpaRepository;
import pe.sigma.model.ComplejidadServicio;
import pe.sigma.model.TipoPrioridad;

public interface IComplejidadServicioDao extends JpaRepository<ComplejidadServicio, Integer> {

}
