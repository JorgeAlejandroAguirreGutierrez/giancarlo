package pe.sigma.dao;

import org.springframework.data.jpa.repository.JpaRepository;

import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import pe.sigma.model.Solicitud;

import java.util.List;

public interface ISolicitudDao extends JpaRepository<Solicitud, Integer>, JpaSpecificationExecutor<Solicitud> {


}
